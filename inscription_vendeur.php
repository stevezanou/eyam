<?php
require('Traitements/session_verif.php');
require('Traitements/bdd.php');
?>
<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>e-Yam: African Online Market</title>

    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!--On rajoute un lien CSS -->
    <link rel="stylesheet" type="text/css" href="css/style_popup.css">

    <!-- Custom styles for this template -->
    <link href="css/shop-homepage.css" type="text/css" rel="stylesheet">

  </head>

  <body>

    <!-- Navigation -->
    <?php include 'header.php'; ?>


    <!-- Page Content -->

    
      
    
    <div class="container">


          

      <div class="row">




        <!-- /.col-lg-3 -->

        <div class="col-lg-9">


      <h1>Formulaire d'inscription </h1>

      <form method="post" action="Traitements/traitement_inscription.php">
      
      <div class="informations">

      <p>
        <label for="nom"> Nom: </label>
        <input type="text" name="nom" id="nom" placeholder="DEEN" required>
      </p>
        <input type ="hidden" name="isVendeur" value="vendeur">

      <p>
        <label for="prenom"> Prénom: </label>
        <input type="text" name="prenom" id="prenom" placeholder="Tamrath" required>
      </p>

      <p>
        <label for="naissance"> Date de naissance: </label>
        <input type="date" name="date_de_naissance" id="naissance">
      </p>

      <p>
        <label for="tel"> Numéro de téléphone: </label>
        <input type="tel" name="telephone" id="tel" maxlength="8" placeholder="00000000" required>
      </p>
      

      <p>
        <label for="mail"> Votre adresse-mail: </label>
        <input type="email" name="email" id="mail" placeholder="tamrathdeen@gmail.com">
      </p>

      <p>
        <!--<label for="adresse"> Adresse: </label> -->
        <p>Adresse:</p>
        <textarea  name="adresse" id="adresse" placeholder="Fridjrossè vers plage" rows="5" cols="40" required ></textarea>
      </p>

      <p>
        <label for="pays"> Pays de résidence: </label>
        <select name="pays" id="pays">
          <option value="france"> France </option>
          <option value="benin" selected> Bénin </option>
          <option value="nigeria"> Nigéria </option>
          <option value="cote_d_ivoire"> Côte d'ivoire </option>
          <option value="togo"> Togo </option>

         </select>  
      </p>

      <p>
        <label for="mot_de_passe"> Mot de passe: </label>
        <input type="password" name="password" id="mot_de_passe" maxlength="8" placeholder="********" required>
      </p>

      <p>
        <label for="mot_de_passe"> Mot de passe à nouveau: </label>
        <input type="password" name="password2" id="mot_de_passe" maxlength="8" placeholder="********" required>
      </p>

      <p>
        <input type="submit" value=" Créer mon compte vendeur" >
      </p>

      </div>

    </form>

        </div>
        <!-- /.col-lg-9 -->

      </div>
      <!-- /.row -->

    </div>
    <!-- /.container -->

    <!-- Footer -->
    <footer class="py-5 bg-dark">

      <div class="bas_page">

      <div class="rubrique"> 

        <h5>Nous connaître</h5>

        <ul>
          <li> Aide </li>
          <li>Contact</li>
          <li>Acheter sur e-yam</li>
          <li>Politique de retour</li>
          <li>Chantal (Notre Bot)</li>
        </ul>
        
      </div>



      <div class="rubrique"> 

        <h5>Devenir partenaire</h5>

        <ul>
          <li> Point relais </li>
          <li>Ambassadeur</li>
          <li><a href="inscription_vendeur.php">Fournisseur</a></li>

        </ul>
        
      </div>




      <div class="rubrique"> 

        <h5>Réseaux sociaux</h5> 

        <ul>
          <li> <img src="Images/facebook.png" class="emoticones"> </li>
          <li> <img src="Images/whatsapp.png" class="emoticones"> </li>
          <li> <img src="Images/twitter.png" class="emoticones"> </li>
          <li> <img src="Images/instagram.png" class="emoticones"> </li>
          <li> <img src="Images/youtube.png" class="emoticones"></li>
        </ul>

      </div>


      <div class="rubrique"> 

        <h5>Mode de paiement</h5>
        

        <ul>
          <li> <img src="Images/billets.png" class="emoticones"> Espèces </li>
          <li> <img src="Images/mobile.png" class="emoticones"> Mobile Money</li>
          <li> <img src="Images/paypal.png" class="emoticones"> Paypal</li>
          <li> <img src="Images/visa.png" class="emoticones"> Visa</li>
          <li> <img src="Images/mastercard.png" class="emoticones"> Master Card</li>
        </ul>


      </div>
        
      <div class="rubrique"> 
        
        <h5> e-Yam international </h5>
        

        <ul>
          <li>France</li>
          <li>Bénin </li>
          <li>Togo</li>
          <li>Nigéria</li>
          <li>Côte d'ivoire</li>
        </ul>

      </div>
 


    </div>


      <div class="container_1">
        <p class="m-0 text-center text-white">Copyright &copy; e-Yam 2018</p>
      </div>  


      <!-- /.container -->
    </footer>

    <!-- Bootstrap core JavaScript -->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- On relie avec notre fichier contenant le javaScript -->
    <script src="code_pup.js" type="text/javascript"></script>



  </body>

</html>
