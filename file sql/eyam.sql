-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Client :  127.0.0.1
-- Généré le :  Sam 16 Mars 2019 à 19:53
-- Version du serveur :  5.7.14
-- Version de PHP :  7.0.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `eyam`
--

-- --------------------------------------------------------

--
-- Structure de la table `article`
--

CREATE TABLE `article` (
  `id` varchar(255) NOT NULL,
  `nom_article` varchar(255) NOT NULL,
  `stock_article` int(11) NOT NULL,
  `photo_article` blob NOT NULL,
  `prix_unitaire` float NOT NULL,
  `description` text NOT NULL,
  `categorie` int(11) NOT NULL,
  `quantite_min` varchar(255) NOT NULL,
  `promotion` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `article`
--

INSERT INTO `article` (`id`, `nom_article`, `stock_article`, `photo_article`, `prix_unitaire`, `description`, `categorie`, `quantite_min`, `promotion`) VALUES
('REF0001', 'ARACHIDE', 20, 0x696d67382e706e67, 20, 'Arachide', 1, '300', 0),
('REF0004', 'BISSAP', 100, 0x696d6733312e706e67, 50, 'JUS DE BISSAP', 6, '300', 20),
('REF0007', 'Huile de palme', 20, 0x696d6735302e706e67, 20, 'Huile de palme en boite', 1, '300', 0),
('REF0008', 'TOMATE', 199, 0x54455354, 200, 'Boite de tomate', 1, '200', 0),
('REF0009', 'PIMENT', 199, 0x54455354, 200, 'Boite de piment', 1, '200', 0);

-- --------------------------------------------------------

--
-- Structure de la table `categorie`
--

CREATE TABLE `categorie` (
  `id` int(11) NOT NULL,
  `nom` varchar(255) CHARACTER SET latin1 NOT NULL,
  `nom_image` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `categorie`
--

INSERT INTO `categorie` (`id`, `nom`, `nom_image`) VALUES
(1, 'Huiles-Épices et Graines', 'Huiles.PNG'),
(2, 'Féculents-Céréales', 'Feculents_Cereales.PNG'),
(3, 'Fruits-Légumes', 'Fruits_Legumes.PNG'),
(4, 'Viandes-Poissons-Oeufs', 'Viandes_Poissons.PNG'),
(5, 'Divers articles', 'Divers.PNG'),
(6, 'Liqueurs-eau-boissons sucrées', 'Boissons.PNG');

-- --------------------------------------------------------

--
-- Structure de la table `chiffre_daffaire`
--

CREATE TABLE `chiffre_daffaire` (
  `id` int(11) NOT NULL,
  `date_ca` datetime NOT NULL,
  `montant_ca` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `commande`
--

CREATE TABLE `commande` (
  `id_cmd` int(11) NOT NULL,
  `date_cmd` date NOT NULL,
  `time_cmd` time NOT NULL,
  `nom_prenom_cmd` varchar(255) NOT NULL,
  `adresse_cmd` text NOT NULL,
  `telephone_cmd` int(11) NOT NULL,
  `montant_cmd` float NOT NULL,
  `panier_cmd` text NOT NULL,
  `statut_cmd` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `commande`
--

INSERT INTO `commande` (`id_cmd`, `date_cmd`, `time_cmd`, `nom_prenom_cmd`, `adresse_cmd`, `telephone_cmd`, `montant_cmd`, `panier_cmd`, `statut_cmd`) VALUES
(2, '2018-12-01', '00:00:00', 'TOTO_TOTO', 'Cotonou', 97119090, 7200, ' Fraise 2000 1// Test1 2000 1// Tes1 2000 1//', ''),
(3, '2018-12-01', '00:00:00', 'titi_titi', 'AKPAKPA', 64967148, 54567.6, '_DSDCVFSD_43453_1//_banane_20_1//_FDD_2000_1//', ''),
(7, '2018-12-01', '00:00:00', 'Matrix_Neo', 'Zoca', 23456789, 15960, 'FDD_2000_1<br/>huile de palme_3000_1<br/>Mil_2000_1<br/>COCA COLA_2000_3<br/>Savon _300_1<br/>', 'Livre'),
(8, '2018-12-03', '00:00:00', 'Mulan_SHISHI', 'Apkapka', 98541230, 9285.6, 'ZERTRTYUUII_2000_1<br/>fd_49_1<br/>ZEZE_3689_1<br/>Concombre_2000_1<br/>', 'Traite'),
(9, '2018-12-03', '00:00:00', 'UCHIWA_Sasuke', 'Ahidjedo', 97451248, 11312.4, 'ZERTRTYUUII_2000_1<br/>fd_49_1<br/>ZEZE_3689_2<br/>', 'En attente'),
(10, '2018-12-03', '00:00:00', 'UZUMAKI_Naruto', 'Haie vive', 98541263, 15739.2, 'ZERTRTYUUII_2000_1<br/>fd_49_1<br/>ZEZE_3689_3<br/>', 'Traite'),
(11, '2018-12-05', '22:54:53', 'NAMIKAZE_Minato', 'Apkapka', 98765456, 59343.6, 'DSDCVFSD_43453_1<br/>Concombre_2000_1<br/>Concombre_2000_1<br/>Carotte_2000_1<br/>', 'Livre'),
(12, '2018-10-09', '10:00:00', 'OBITO_UCHIWA', 'KONOHA', 752219785, 22987, 'TOMATE_4000_1', 'En attente'),
(13, '2018-08-09', '14:00:00', 'YAKUSHI_KABUTO', 'KONOHA', 745434543, 40000, 'TOMATE_4000_2', 'En attente'),
(14, '2018-04-02', '09:00:00', 'ZETSU', 'AKATSUKI', 237659876, 40000, 'TOMATE_3000_1', 'En attente');

-- --------------------------------------------------------

--
-- Structure de la table `inscrit`
--

CREATE TABLE `inscrit` (
  `id` int(11) NOT NULL,
  `nom` varchar(255) NOT NULL,
  `prenom` varchar(255) NOT NULL,
  `date_de_naissance` date NOT NULL,
  `telephone` varchar(255) NOT NULL,
  `adresse` text NOT NULL,
  `pays` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL,
  `pointDeFidelite` int(11) NOT NULL DEFAULT '0',
  `isAdmin` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `inscrit`
--

INSERT INTO `inscrit` (`id`, `nom`, `prenom`, `date_de_naissance`, `telephone`, `adresse`, `pays`, `email`, `password`, `type`, `pointDeFidelite`, `isAdmin`) VALUES
(1, 'toto', 'toto', '0000-00-00', '', '', '', 'toto@gmail.com', 'f71dbe52628a3f83a77ab494817525c6', '', 0, 0),
(2, 'tutu', 'tutu', '0000-00-00', '', '', '', 'tutu@gmail.com', 'bdb8c008fa551ba75f8481963f2201da', '', 0, 0),
(3, 'jaja', 'jaja', '0000-00-00', '', '', '', 'jaja@gmail.com', 'bb0ed6ad56f41c6de469776171261226', '', 0, 0),
(4, 'titi', 'titi', '0000-00-00', '', '', '', 'titi@titi.fr', '5d933eef19aee7da192608de61b6c23d', '', 0, 0),
(5, 'zaza', 'zaza', '1995-07-10', '07654398', 'calavi', 'benin', 'zaza@gmail.com', '8ba97607a1485ccdbe19745ed80cd52d', '', 0, 1),
(6, 'zozo', 'deci', '1898-07-10', '67987543', 'Apkapka', 'benin', 'deci@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', '', 0, 0),
(7, 'MICHEL', 'JEAN', '1995-10-10', '12345678', 'kpota', 'benin', 'vendeur@gmail.com', '34fdd771c0b05faaf5f16b3b0ea12702', 'vendeur', 0, 0),
(8, 'juju', 'juju', '1995-07-10', '67543245', 'plage', 'benin', 'juju@gmail.com', '0348dcd774a2892097b9d5c84ce882d3', 'client', 0, 0),
(9, 'mimi', 'mumu', '1995-07-12', '0754637876', 'Akassato', 'Mali', 'mumu@gmail.com', '$2y$10$FL5fWMtnHtjbuWGDE9py0uRkwJkD/cvzjjMEFVxuUvO5/fxCnLlYm', 'client', 0, 0),
(21, 'momo', 'momo', '1991-12-12', '77474747', 'momo', 'benin', 'momo@gmail.com', '$2y$10$z8hXcW1Aq3GPEXZ5hT.B5OMQqxyJ3x/clmD0yyGja8KDlTi9yLGM6', 'client', 0, 0),
(22, 'mama', 'mama', '2012-12-12', '02154895', 'mama', 'benin', 'mama@gmail.com', '$2y$10$PHgwtPn8DXO6.DEogZCa/.z3KY3oDVwhjBPs0HJCz.hSVZGyBN1ya', 'client', 0, 0);

-- --------------------------------------------------------

--
-- Structure de la table `vendeurs`
--

CREATE TABLE `vendeurs` (
  `id_vend` int(11) NOT NULL,
  `nom_vend` int(11) NOT NULL,
  `prenom_vend` int(11) NOT NULL,
  `photo_vend` blob NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `vendeur_article`
--

CREATE TABLE `vendeur_article` (
  `vendeur_id` int(11) NOT NULL,
  `article_id` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Index pour les tables exportées
--

--
-- Index pour la table `article`
--
ALTER TABLE `article`
  ADD PRIMARY KEY (`id`),
  ADD KEY `categorie` (`categorie`);

--
-- Index pour la table `categorie`
--
ALTER TABLE `categorie`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`);

--
-- Index pour la table `chiffre_daffaire`
--
ALTER TABLE `chiffre_daffaire`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `commande`
--
ALTER TABLE `commande`
  ADD PRIMARY KEY (`id_cmd`);

--
-- Index pour la table `inscrit`
--
ALTER TABLE `inscrit`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `vendeurs`
--
ALTER TABLE `vendeurs`
  ADD PRIMARY KEY (`id_vend`);

--
-- Index pour la table `vendeur_article`
--
ALTER TABLE `vendeur_article`
  ADD PRIMARY KEY (`vendeur_id`,`article_id`),
  ADD KEY `vendeur_id` (`vendeur_id`),
  ADD KEY `article_id` (`article_id`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `categorie`
--
ALTER TABLE `categorie`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT pour la table `chiffre_daffaire`
--
ALTER TABLE `chiffre_daffaire`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `commande`
--
ALTER TABLE `commande`
  MODIFY `id_cmd` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT pour la table `inscrit`
--
ALTER TABLE `inscrit`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT pour la table `vendeurs`
--
ALTER TABLE `vendeurs`
  MODIFY `id_vend` int(11) NOT NULL AUTO_INCREMENT;
--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `article`
--
ALTER TABLE `article`
  ADD CONSTRAINT `CATEGORIE` FOREIGN KEY (`categorie`) REFERENCES `categorie` (`id`);

--
-- Contraintes pour la table `vendeur_article`
--
ALTER TABLE `vendeur_article`
  ADD CONSTRAINT `Constr_VendeurArticle_Article_fk` FOREIGN KEY (`article_id`) REFERENCES `article` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `Constr_VendeurArticle_Vendeur_fk` FOREIGN KEY (`vendeur_id`) REFERENCES `vendeurs` (`id_vend`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
