<?php
require('Traitements/session_verif.php');
require('Traitements/bdd.php');
include "classes/Article.php";
include "classes/Categorie.php";
?>


<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>e-Yam: African Online Market</title>

    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/shop-homepage.css" rel="stylesheet">
    <link href="css/style_articles.css" rel="stylesheet">
    <link href="css/style_popup.css" rel="stylesheet">
    <!-- On rajoute le lien avec le css du chat-->
    <link href="css/tuto_chat.css" rel="stylesheet">

    <link href="css/style-cart.css" rel="stylesheet">

    
   
                 
  </head>

  <body>

    

     <!-- Navigation -->
      
    
     <?php //include 'header.php'; ?>


    <!-- Page Content 
   

    <div class="container">-->

          <?php
            // get database connection
            $database = new Database();
            $bdd = $database->getConnection();

            $article = new Article($bdd);
            $category = new Categorie($bdd);
            

            if(isset($_GET['categorie']))
            {
              echo "on entre avec id set 1 ";
              //On affiche les articles de la categorie selectionnée
              $categorie = $_GET['categorie'];
              

              $stmt = $category->readOne($categorie);
              $num = $stmt->rowCount();

             // $donnees = $req->fetch();
              //var_dump($donnees);

              if($num !=0 )
								{
                  //echo '<h1 class="my-4 text-center text-lg-left">'.$donnees["nom"].'</h1>';
                  //echo '<div class="row text-center text-lg-left">';
                  echo '<div class="row">';
                    echo  '<div class="tous_articles">';
                    //Partie du panier
                    $status = "";
                    if(isset($_POST['id']) && $_POST['id']!="")
                    {
                      /*$req2=$bdd->prepare('SELECT * FROM article WHERE categorie=:categorie AND id = :id');
                      $req2->execute(array(
                      'categorie'=>$categorie,
                      'id'=>$_POST['id'],
                      ));*/

                      $row=$article->readForCart($categorie, $_POST['id']);
                      //var_dump($stmt);
                      
                     
                      //var_dump($row);
                      $name = $row['nom_article'];
                      $id = $row['id'];
                      $price = $row['prix_unitaire'];
                      $image = $row['photo_article'];

                      $cartArray = array(
                        $id=>array(
                        'name'=>$name,
                        'id'=>$id,
                        'price'=>$price,
                        'quantity'=>1,
                        'image'=>$image)
                      );

                      if(empty($_SESSION["shopping_cart"])) {
                        $_SESSION["shopping_cart"] = $cartArray;
                        $status = "<div class='box'>Produit ajouté à votre panier</div>";
                      }
                      else
                      {
                          $array_keys = array_keys($_SESSION["shopping_cart"]);
                          if(in_array($id,$array_keys)) 
                          {
                            $status = "<div class='box' style='color:red;'>
                            Ce produit a déjà été ajouté</div>";	
                          } 
                          else 
                          {

                            $_SESSION["shopping_cart"] = array_merge(
                            $_SESSION["shopping_cart"],
                            $cartArray
                            );
                            $status = "<div class='box'>Produit ajouté à votre panier</div>";
                          }
                    
                      }
                    }
                    include 'header.php';
                
                    $nbArt=$article->countPage($categorie);//Retourne le nombre d'articles selon la catégorie que l'utilisateur a sélectionné
                    $perPage = 14;//Le nombre d'articles qu'on veut afficher par page
                   
                   
                    $nbPage = ceil($nbArt/$perPage);//Pour avoir le nombre total de page, on divise le nombre d'articles par le nombre total de page

                    if(isset($_GET['page']) && $_GET['page']>0 && $_GET['page']<= $nbPage)
                    {
                      $cPage = ($_GET['page']);
                    }
                    else{
                      $cPage=1;
                    }

                    $from_record_num = ($cPage-1)*$perPage;
                   
                    $stmt=$article->readForPage($from_record_num, $perPage, $categorie);//Methode qui retourne le nombre d'article selon la catégorie que l'utiilisateur à sélectionné

                                      
                 //var_dump($req2->fetchAll());
                  while ($donnees2 = $stmt->fetch())//Fecth sur le résultat de la méthode
									{
                                        
                    echo    '<div class="objet" >';
                          
                    echo      '<div class="article " >
                                <form method="post" action="">
                                <input type="hidden" name="id" value='.$donnees2['id'].' />
                                <button type="submit" id="close-image" > <img id="Image" src="Images/articles/'.$donnees2['photo_article'].'"></button>
                                <!--<button type="submit" id="close-image" > <img id="Image"  src="data:image/jpeg;base64,'.base64_encode( $donnees2['photo_article'] ).'"></button>-->
                                  <div id="infos" >
                                    <h1 id="titre" style="font-size:25px; text-transform: uppercase;">'. $donnees2['nom_article'].'</h1>
                                    <h4 id="prix" style="font-size:25px; ">'. $donnees2['prix_unitaire'].' FCFA'.'</h1>
                                   
                                    
                                  </div>
                                  
                                </form>
                              </div>';

                    echo    '</div>';
                    
                    //echo '<br/>';
                   
                  }
                  
                  echo  '</div>';
                  
                  echo '</div>';
                  
                  //echo  '</div>';

                }
                //Affiche le nombre de pages
                echo '<nav  class="pagin">
                      <ul class="pagination">
                        <li class="page-item"><a class="page-link" href="article?categorie='.$categorie.'&page='.($cPage-1).'">Prec</a></li>';
                        for($i=1;$i<=$nbPage;$i++)
                         {
                          echo '<li class="page-item"><a class="page-link" href="article?categorie='.$categorie.'&page='.$i.'">'.$i.'</a></li>';
                         }
                         echo '<li class="page-item"><a class="page-link" href="#">Suiv</a></li>
                      </ul>
                    </nav>';
                    //echo '<div style="text-align:center;"><a href="article?categorie='.$categorie.'&page='.$i.'">'.$i.'</a></div>';
                 

            }
            else
            {

              echo "on entre";

              
                  //echo '<h1 class="my-4 text-center text-lg-left">'.$donnees["nom"].'</h1>';
                  //echo '<div class="row text-center text-lg-left">';
                  echo '<div class="row">';
                    echo  '<div class="tous_articles">';
                    //Partie du panier
                    $status = "";
                    if(isset($_POST['id']) && $_POST['id']!="")
                    {
                      $req2=$bdd->prepare('SELECT * FROM article WHERE id = :id');
                      $req2->execute(array(
                      
                      'id'=>$_POST['id'],
                      ));
                      
                      
                      $row = $req2->fetch();
                      $name = $row['nom_article'];
                      $id = $row['id'];
                      $price = $row['prix_unitaire'];
                      $image = $row['photo_article'];

                      $cartArray = array(
                        $id=>array(
                        'name'=>$name,
                        'id'=>$id,
                        'price'=>$price,
                        'quantity'=>1,
                        'image'=>$image)
                      );

                      if(empty($_SESSION["shopping_cart"])) {
                        $_SESSION["shopping_cart"] = $cartArray;
                        $status = "<div class='box'>Produit ajouté à votre panier</div>";
                      }
                      else
                      {
                          $array_keys = array_keys($_SESSION["shopping_cart"]);
                          if(in_array($id,$array_keys)) 
                          {
                            $status = "<div class='box' style='color:red;'>
                            Ce produit a déjà été ajouté</div>";	
                          } 
                          else 
                          {

                            $_SESSION["shopping_cart"] = array_merge(
                            $_SESSION["shopping_cart"],
                            $cartArray
                            );
                            $status = "<div class='box'>Produit ajouté à votre panier</div>";
                          }
                    
                      }
                    }
                    include 'header.php';
                    //Partie pagination
                    $req3=$bdd->prepare('SELECT COUNT(id) AS nbArt FROM article ');
                    $req3->execute(array(
                    
                    ));
                    $donnees_count = $req3->fetch();
                    $nbArt = $donnees_count['nbArt'];
                    $perPage = 14;
                    
                    $nbPage = ceil($nbArt/$perPage);

                    if(isset($_GET['page']) && $_GET['page']>0 && $_GET['page']<= $nbPage)
                    {
                      $cPage = ($_GET['page']);
                    }
                    else{
                      $cPage=1;
                    }

                    //Ici on affiche tous les articles qui sont inclus dans la catégorie que l'utilisateur sélectionne
                    $req2=$bdd->prepare('SELECT * FROM article LIMIT '.(($cPage-1)*$perPage).','.$perPage.'');
                    $req2->execute(array());

                   
                 //var_dump($req2->fetchAll());
                  while ($donnees2 = $req2->fetch())
									{
                                        
                    echo    '<div class="objet" >';
                          
                    echo      '<div class="article " >
                                <form method="post" action="">
                                <input type="hidden" name="id" value='.$donnees2['id'].' />
                                <button type="submit" id="close-image" > <img id="Image" src="Images/articles/'.$donnees2['photo_article'].'"></button>
                                <!--<button type="submit" id="close-image" > <img id="Image"  src="data:image/jpeg;base64,'.base64_encode( $donnees2['photo_article'] ).'"></button>-->
                                  <div id="infos" >
                                    <h1 id="titre" style="font-size:25px; text-transform: uppercase;">'. $donnees2['nom_article'].'</h1>
                                    <h4 id="prix" style="font-size:25px; ">'. $donnees2['prix_unitaire'].' FCFA'.'</h1>
                                   
                                    
                                  </div>
                                  
                                </form>
                              </div>';

                    echo    '</div>';
                    
                    //echo '<br/>';
                   
                  }
                  
                  echo  '</div>';
                  
                  echo '</div>';
                  
                  //echo  '</div>';

                //}
                
                echo '<nav  class="pagin">
                      <ul class="pagination">
                        <li class="page-item"><a class="page-link" href="article?page='.($cPage-1).'">Prec</a></li>';
                        for($i=1;$i<=$nbPage;$i++)
                         {
                          echo '<li class="page-item"><a class="page-link" href="article?page='.$i.'">'.$i.'</a></li>';
                         }
                         echo '<li class="page-item"><a class="page-link" href="#">Suiv</a></li>
                      </ul>
                    </nav>';


            }
          
          
          ?>
            <div style="clear:both;"></div>
 
            <div class="message_box" style="margin:10px 0px;">
            <?php //echo $status; ?>
            </div>
    <!-- </div>
    /.container -->

    <!-- Footer -->
    <footer class="py-5 bg-dark">

      <div class="bas_page">

      <div class="rubrique"> 

        <h5>Nous connaître</h5>

        <ul>
          <li> Aide </li>
          <li>Contact</li>
          <li>Acheter sur e-yam</li>
          <li>Politique de retour</li>
          <li>Chantal (Notre Bot)</li>
        </ul>
        
      </div>



      <div class="rubrique"> 

        <h5>Devenir partenaire</h5>

        <ul>
          <li> Point relais </li>
          <li>Ambassadeur</li>
          <li><a href="inscription_vendeur.php">Fournisseur</a></li>

        </ul>
        
      </div>




      <div class="rubrique"> 

        <h5>Réseaux sociaux</h5> 

        <ul>
          <li> <img src="Images/facebook.png" class="emoticones"> </li>
          <li> <img src="Images/whatsapp.png" class="emoticones"> </li>
          <li> <img src="Images/twitter.png" class="emoticones"> </li>
          <li> <img src="Images/instagram.png" class="emoticones"> </li>
          <li> <img src="Images/youtube.png" class="emoticones"></li>
        </ul>

      </div>


      <div class="rubrique"> 

        <h5>Mode de paiement</h5>
        

        <ul>
          <li> <img src="Images/billets.png" class="emoticones"> Espèces </li>
          <li> <img src="Images/mobile.png" class="emoticones"> Mobile Money</li>
          <li> <img src="Images/paypal.png" class="emoticones"> Paypal</li>
          <li> <img src="Images/visa.png" class="emoticones"> Visa</li>
          <li> <img src="Images/mastercard.png" class="emoticones"> Master Card</li>
        </ul>

      </div>
        
      <div class="rubrique"> 
        
        <h5> e-Yam international </h5>
        

        <ul>
          <li>France</li>
          <li>Bénin </li>
          <li>Togo</li>
          <li>Nigéria</li>
          <li>Côte d'ivoire</li>
        </ul>

      </div>
 


    </div>


      <div class="container_1">
        <p class="m-0 text-center text-white">Copyright &copy; e-Yam 2018</p>
      </div>  


      <!-- /.container -->
    </footer>

    <!-- Bootstrap core JavaScript -->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
     <!-- On rajoute ceci pour l'ouverture des popups -->
     <script src="code_pup.js"></script>
     
      
  </body>

</html>
