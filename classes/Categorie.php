<?php
class Categorie{
 
    // database connection and table name
    private $conn;
    private $table_name = "categorie";
 
    // object properties
    public $id;
    public $nom;
    public $nom_image;
   
 
    public function __construct($db)
    {
        $this->conn = $db;
    }
 
    // used by select drop-down list
    public function read(){
        //select all data
        $query = "SELECT
                    id, nom, nom_image
                FROM
                    " . $this->table_name . "
                ORDER BY
                    nom";
 
        $stmt = $this->conn->prepare($query);
        $stmt->execute();
 
        return $stmt;
    }

    public function readOne($id){
        //select all data
        $query = "SELECT
                    id, nom, nom_image
                FROM
                    " . $this->table_name . "
                    WHERE id = ?
                ORDER BY
                    nom";
 
       
        // prepare query statement
        $stmt = $this->conn->prepare( $query );
    
        // bind id of product to be updated
        $stmt->bindParam(1, $id);
    
        // execute query
        $stmt->execute();
 
        return $stmt;
    }

    
}
?>