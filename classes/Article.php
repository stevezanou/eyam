<?php
class Article{
 
    // database connection and table name
    private $conn;
    private $table_name = "article";
 
    // object properties
    public $id;
    public $nom_article;
    public $stock_article;
    public $photo_article;
    public $prix_unitaire;
    public $description;
    public $categorie;
    public $quantite_min;
    public $promotion;
 
    // constructor with $db as database connection
    public function __construct($db){
        $this->conn = $db;
    }


    // read products
    function read(){
    
        // select all query
        $query = "SELECT
                    c.nom as category_name, p.id, p.nom_article, p.stock_article, p.photo_article, p.prix_unitaire, p.description, p.categorie, p.quantite_min, p.promotion
                FROM
                    " . $this->table_name . " p
                    LEFT JOIN
                        categorie c
                            ON p.categorie = c.id
                ORDER BY
                    p.id DESC";
    
        // prepare query statement
        $stmt = $this->conn->prepare($query);
    
        // execute query
        $stmt->execute();
        var_dump($this->conn);
    
        return $stmt;
    }

    function create(){
 
        // query to insert record
        $query = "INSERT INTO
                    " . $this->table_name . "
                SET
                id=:id, nom_article=:nom_article, stock_article=:stock_article, photo_article=:photo_article, prix_unitaire=:prix_unitaire, description=:description, categorie=:categorie, quantite_min=:quantite_min, promotion=:promotion";
     
        // prepare query
        $stmt = $this->conn->prepare($query);
     
        // sanitize
        $this->id=htmlspecialchars(strip_tags($this->id));
        $this->nom_article=htmlspecialchars(strip_tags($this->nom_article));
        $this->stock_article=htmlspecialchars(strip_tags($this->stock_article));
        $this->photo_article=htmlspecialchars(strip_tags($this->photo_article));
        $this->prix_unitaire=htmlspecialchars(strip_tags($this->prix_unitaire));
        $this->description=htmlspecialchars(strip_tags($this->description));
        $this->categorie=htmlspecialchars(strip_tags($this->categorie));
        $this->quantite_min=htmlspecialchars(strip_tags($this->quantite_min));
        $this->promotion=htmlspecialchars(strip_tags($this->promotion));
     
        // bind values
        $stmt->bindParam(":id", $this->id);
        $stmt->bindParam(":nom_article", $this->nom_article);
        $stmt->bindParam(":stock_article", $this->stock_article);
        $stmt->bindParam(":photo_article", $this->photo_article);
        $stmt->bindParam(":prix_unitaire", $this->prix_unitaire);
        $stmt->bindParam(":description", $this->description);
        $stmt->bindParam(":categorie", $this->categorie);
        $stmt->bindParam(":quantite_min", $this->quantite_min);
        $stmt->bindParam(":promotion", $this->promotion);
        
        var_dump($stmt);
        // execute query
        if($stmt->execute()){
            return true;
        }
     
        return false;
         
    }

    // used when filling up the update product form
function readOne(){
 
    // query to read single record
    $query = "SELECT
                c.nom as category_name, p.id, p.nom_article, p.stock_article, p.photo_article, p.prix_unitaire, p.description, p.categorie, p.quantite_min, p.promotion
            FROM
                " . $this->table_name . " p
                LEFT JOIN
                    categorie c
                        ON p.categorie = c.id
            WHERE
                p.id = ?
            LIMIT
                0,1";
 
    // prepare query statement
    $stmt = $this->conn->prepare( $query );
 
    // bind id of product to be updated
    $stmt->bindParam(1, $this->id);
 
    // execute query
    $stmt->execute();
 
    // get retrieved row
    $row = $stmt->fetch(PDO::FETCH_ASSOC);
 
    // set values to object properties

        $this->nom_article= $row['nom_article'];
        $this->stock_article=$row['stock_article'];
        $this->photo_article=$row['photo_article'];
        $this->prix_unitaire=$row['prix_unitaire'];
        $this->description=$row['description'];
        $this->categorie=$row['categorie'];
        $this->quantite_min=$row['quantite_min'];
        $this->promotion=$row['promotion'];
}


// search products
function search($keywords){
 
    // select all query
    $query = "SELECT
                c.nom as category_name, p.id, p.nom_article, p.stock_article, p.photo_article, p.prix_unitaire, p.description, p.categorie, p.quantite_min, p.promotion
            FROM
                " . $this->table_name . " p
                LEFT JOIN
                    categorie c
                        ON p.categorie = c.id
            WHERE
                p.nom_article LIKE ?
            ORDER BY
                p.id DESC";
 
    // prepare query statement
    $stmt = $this->conn->prepare($query);
 
    // sanitize
    $keywords=htmlspecialchars(strip_tags($keywords));
    $keywords = "%{$keywords}%";
 
    // bind
    $stmt->bindParam(1, $keywords,PDO::PARAM_STR);
  
    // execute query
    $stmt->execute();

    //$rows = $stmt;

    //var_dump($rows);
 
    return $stmt;
}


// read all products by categorie and with the number of product by page
function readForPage($from_record_num, $records_per_page, $categorie){
   
    // select all products query
    $query = "SELECT
                *
            FROM
                " . $this->table_name . "
            WHERE
            categorie LIKE ?
            LIMIT
                ?, ?";
 
    // prepare query statement
    $stmt = $this->conn->prepare( $query );

    // sanitize
    $keywords=htmlspecialchars(strip_tags($categorie));
    $keywords = "%{$categorie}%";
 
    // bind limit clause variables
    $stmt->bindParam(1, $keywords);
    $stmt->bindParam(2, $from_record_num, PDO::PARAM_INT);
    $stmt->bindParam(3, $records_per_page, PDO::PARAM_INT);
 
    // execute query
    $stmt->execute();
 
    // return values
    return $stmt;
}


// read all only by categorie 
function readForPageByCategory($categorie){
   
    // select all products query
    $query = "SELECT
                *
            FROM
                " . $this->table_name . "
            WHERE
            categorie LIKE ?";
 
    // prepare query statement
    $stmt = $this->conn->prepare( $query );

    // sanitize
    $keywords=htmlspecialchars(strip_tags($categorie));
    $keywords = "%{$categorie}%";
 
    // bind limit clause variables
    $stmt->bindParam(1, $keywords);
 
    // execute query
    $stmt->execute();
 
    // return values
    return $stmt;
}


// used for paging products
public function countPage($categorie){
 
    // query to count all product records
    $query = "SELECT count(*) FROM " . $this->table_name ." WHERE categorie LIKE ? ";
 
    // prepare query statement
    $stmt = $this->conn->prepare( $query );

    $stmt->bindParam(1, $categorie, PDO::PARAM_INT);
 
    // execute query
    $stmt->execute();
 
    // get row value
    $rows = $stmt->fetch(PDO::FETCH_NUM);
 
    // return count
    return $rows[0];
}

//Utilisé pour la partie du panier.
//Pour remplir le panier
public function readForCart($categorie, $id){
 
    // selectionne l'article selon ce que l'utilisateur a choisi
     
     $query = "SELECT
                    *
                FROM
                    " . $this->table_name . "
                WHERE
                categorie LIKE ? AND id LIKE ? ";
 
    // prepare query statement
    $stmt = $this->conn->prepare( $query );

    // sanitize
    $keywords=htmlspecialchars(strip_tags($categorie));
    $keywords = "%{$categorie}%";

    $keywords2=htmlspecialchars(strip_tags($id));
    $keywords2 = "%{$id}%"; 
 
    // bind limit clause variables
    $stmt->bindParam(1, $keywords);
    $stmt->bindParam(2, $keywords2);
 
    // execute query
    $stmt->execute();
    
    // get row value
    $rows = $stmt->fetch();
    //var_dump($rows);
 
    // return count
    return $rows;
}



}