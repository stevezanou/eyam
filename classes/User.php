<?php

Class User {

            // database connection and table name
            private $conn;
            private $table_name = "inscrit";

            public $id;
            public $nom;
            public $prenom;
            public $date_de_naissance;
            public $telephone;
            public $adresse;
            public $pays;
            public $email;
            public $password;
            public $type;
            public $pointDeFidelite;
            public $isAdmin;
            
            // constructor
            public function __construct($db){
                $this->conn = $db;
            }


            // create new user record
            function create(){
            
                // insert query
                $query = "INSERT INTO " . $this->table_name . "
                        SET
                            nom = :nom,
                            prenom = :prenom,
                            date_de_naissance = :date_de_naissance,
                            telephone = :telephone,
                            adresse = :adresse,
                            pays = :pays,
                            email = :email,
                            password = :password,
                            type = :type";
            
                // prepare the query
                $stmt = $this->conn->prepare($query);
            
                // sanitize
                $this->nom=htmlspecialchars(strip_tags($this->nom));
                $this->prenom=htmlspecialchars(strip_tags($this->prenom));
                $this->date_de_naissance=htmlspecialchars(strip_tags($this->date_de_naissance));
                $this->telephone=htmlspecialchars(strip_tags($this->telephone));
                $this->adresse=htmlspecialchars(strip_tags($this->adresse));
                $this->pays=htmlspecialchars(strip_tags($this->pays));
                $this->email=htmlspecialchars(strip_tags($this->email));
                $this->password=htmlspecialchars(strip_tags($this->password));
                $this->type=htmlspecialchars(strip_tags($this->type));
                
            
                // bind the values
                $stmt->bindParam(':nom', $this->nom);
                $stmt->bindParam(':prenom', $this->prenom);
                $stmt->bindParam(':date_de_naissance', $this->date_de_naissance);
                $stmt->bindParam(':telephone', $this->telephone);
                $stmt->bindParam(':adresse', $this->adresse);
                $stmt->bindParam(':pays', $this->pays);
                $stmt->bindParam(':email', $this->email);
                $stmt->bindParam(':password', $this->password);
                $stmt->bindParam(':type', $this->type);
            
                // hash the password before saving to database
                $password_hash = password_hash($this->password, PASSWORD_BCRYPT);
                $stmt->bindParam(':password', $password_hash);
            
                // execute the query, also check if query was successful
                if($stmt->execute()){
                    return true;
                }
            
                return false;
            }


            // check if given email exist in the database
            function emailExists(){
            
                // query to check if email exists
                $query = "SELECT *
                        FROM " . $this->table_name . "
                        WHERE email = ?
                        LIMIT 0,1";
            
                // prepare the query
                $stmt = $this->conn->prepare( $query );
            
                // sanitize
                $this->email=htmlspecialchars(strip_tags($this->email));
            
                // bind given email value
                $stmt->bindParam(1, $this->email);
            
                // execute the query
                $stmt->execute();
            
                // get number of rows
                $num = $stmt->rowCount();
            
                // if email exists, assign values to object properties for easy access and use for php sessions
                if($num>0){
            
                    // get record details / values
                    $row = $stmt->fetch(PDO::FETCH_ASSOC);
            
                    // assign values to object properties
                    $this->id = $row['id'];
                    $this->nom = $row['nom'];
                    $this->prenom = $row['prenom'];
                    $this->password = $row['password'];
                    $this->type = $row['type'];
                    $this->isAdmin = $row['isAdmin'];
            
                    // return true because email exists in the database
                    return true;
                }
            
                // return false if email does not exist in the database
                return false;
            }
 
            // update() method will be here


}

?>