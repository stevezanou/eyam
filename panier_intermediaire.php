<?php
require('Traitements/session_verif.php');
require('Traitements/bdd.php');
?>
<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>e-Yam: African Online Market</title>

    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!--On rajoute un lien CSS -->
    <link rel="stylesheet" type="text/css" href="css/style_popup.css">

    <!-- Custom styles for this template -->
    <link href="css/shop-homepage.css" type="text/css" rel="stylesheet">
    <link href="css/style-cart.css" rel="stylesheet">
     <!-- On rajoute le lien avec le css des popups-->
     <link href="css/style_popup.css" rel="stylesheet">
    <!-- On rajoute le lien avec le css du chat-->
    <link href="css/tuto_chat.css" rel="stylesheet">

  </head>

  <body>

    <!-- Navigation -->
    <?php include 'header.php'; ?>


    <!-- Page Content -->

    
      
    
    <div class="container">


          

      <div class="row">




        <!-- /.col-lg-3 -->

        <div class="col-lg-9">

      <?php
   
        if(isset($_GET) && !empty($_GET["message"]))
        {
            extract($_GET);
            if($message == "1")
            {
              echo '<p> Remplissez le formulaire ci dessous</p>';

              echo '<p> Vous pouvez aussi vous inscire en cliquant ici </p>';
            }
        }
      ?>
      <h1>Formulaire d'informations </h1>

      <form method="post" action="Traitements/traitement_panier.php">
      
      <div class="informations">

      <p>
        <label for="nom"> Nom: </label>
        <input type="text" name="nom" id="nom" placeholder="DEEN" required>
      </p>

      <p>
        <label for="prenom"> Prénom: </label>
        <input type="text" name="prenom" id="prenom" placeholder="Tamrath" required>
      </p>

      
      <p>
        <label for="tel"> Numéro de téléphone: </label>
        <input type="tel" name="telephone" id="tel" maxlength="8" placeholder="00000000" required>
      </p>
      

      <p>
        <!--<label for="adresse"> Adresse: </label> -->
        <p>Adresse:</p>
        <textarea  name="adresse" id="adresse" placeholder="Fridjrossè vers plage" rows="5" cols="40" required ></textarea>
      </p>

      <p>
        <label for="pays"> Pays de résidence: </label>
        <select name="pays" id="pays">
          <option value="france"> France </option>
          <option value="benin" selected> Bénin </option>
          <option value="nigeria"> Nigéria </option>
          <option value="cote_d_ivoire"> Côte d'ivoire </option>
          <option value="togo"> Togo </option>

         </select>  
      </p>

      <p>
        <input type="submit" value=" Valider" >
      </p>

      </div>

    </form>

        </div>
        <!-- /.col-lg-9 -->

      </div>
      <!-- /.row -->

    </div>
    <!-- /.container -->

    <!-- Footer -->
    <footer class="py-5 bg-dark">

      <div class="bas_page">

      <div class="rubrique"> 

        <h5>Nous connaître</h5>

        <ul>
          <li> Aide </li>
          <li>Contact</li>
          <li>Acheter sur e-yam</li>
          <li>Politique de retour</li>
          <li>Chantal (Notre Bot)</li>
        </ul>
        
      </div>



      <div class="rubrique"> 

        <h5>Devenir partenaire</h5>

        <ul>
          <li> Point relais </li>
          <li>Ambassadeur</li>
          <li>Fournisseur</li>

        </ul>
        
      </div>




      <div class="rubrique"> 

        <h5>Réseaux sociaux</h5> 

        <ul>
          <li> <img src="Images/facebook.png" class="emoticones"> </li>
          <li> <img src="Images/whatsapp.png" class="emoticones"> </li>
          <li> <img src="Images/twitter.png" class="emoticones"> </li>
          <li> <img src="Images/instagram.png" class="emoticones"> </li>
          <li> <img src="Images/youtube.png" class="emoticones"></li>
        </ul>

      </div>


      <div class="rubrique"> 

        <h5>Mode de paiement</h5>
        

        <ul>
          <li> <img src="Images/billets.png" class="emoticones"> Espèces </li>
          <li> <img src="Images/mobile.png" class="emoticones"> Mobile Money</li>
          <li> <img src="Images/paypal.png" class="emoticones"> Paypal</li>
          <li> <img src="Images/visa.png" class="emoticones"> Visa</li>
          <li> <img src="Images/mastercard.png" class="emoticones"> Master Card</li>
        </ul>


      </div>
        
      <div class="rubrique"> 
        
        <h5> e-Yam international </h5>
        

        <ul>
          <li>France</li>
          <li>Bénin </li>
          <li>Togo</li>
          <li>Nigéria</li>
          <li>Côte d'ivoire</li>
        </ul>

      </div>
 


    </div>


      <div class="container_1">
        <p class="m-0 text-center text-white">Copyright &copy; e-Yam 2018</p>
      </div>  


      <!-- /.container -->
    </footer>

    <!-- Bootstrap core JavaScript -->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- On relie avec notre fichier contenant le javaScript -->
    <script src="code_pup.js" type="text/javascript"></script>



  </body>

</html>




















