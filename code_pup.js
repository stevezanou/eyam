
/* Création de la fonction openModal qui rend 
visible le popup de l'onglet votre compte*/
function openModal(){
	document.getElementById("modal").style.top= "0px";
}
/* Création de la fonction closeModal pour fermer
 le popup de l'onglet votre compte*/
function closeModal(){
	document.getElementById("modal").style.top= "-1000px";
}

/* Permet de cacher l'onglet de login client 
accessible à partir du popup modal*/
function closeLog(){
	document.getElementById("log").style.top= "-1000px";
}

/* Permet d'ouvrir le popup de logging pour les clients 
qui possèdent déjà un compte*/
function openLogSite(){
	document.getElementById("log").style.top= "190px";
}


/* Fermerture de modal lorsqu'on enlève le curseur 
ici on écoute les évènements et on réagit
 lorsque la souris quitte le popup*/
 var popu = document.getElementById('modal');
/* var cible=document.getElementById("target");*/

 popu.addEventListener('mouseleave', function(e) {
 	/*cible.innerHTML = 'Vous etes parti !';*/
 	closeModal();
 });

 popu.addEventListener('mouseleave', function(e) {
 	/*cible.innerHTML = 'Vous etes parti !';*/
 	closeModal();
 });


/* Fermerture du popu de loging lorsqu'on enlève 
le curseur ici on écoute les évènements et on réagit
 lorsque la souris quitte le popup*/
 var popu_1 = document.getElementById('log');

 popu_1.addEventListener('mouseleave', function(e) {
 	/*cible.innerHTML = 'Vous etes parti !';*/
 	closeLog();
 });
