<?php
session_start();
//Si le user n'est pas connecté, on invalide la variable logged
if(!isset($_SESSION['logged']))
{
	//header('Location: index.php');
	$_SESSION['logged'] = false;
}



if(isset($_SESSION['nom']) && isset($_SESSION['prenom']))
{
	$nom=$_SESSION['nom'];
	$prenom=$_SESSION['prenom'];
}
if(isset($_SESSION['password']))
{
	$pass=$_SESSION['password'];
}
if(isset($_SESSION['isAdmin']))
{
	$isAdmin=$_SESSION['isAdmin'];
}
if(isset($_SESSION['type']))
{
	$type=$_SESSION['type'];
}

?>