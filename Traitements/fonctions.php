<?php

class Functions {

    //Select from return fecthAll()
    public function selectFromTableFetchAll($bdd,$tableName)
    {
        $req2=$bdd->prepare('SELECT * FROM '.$tableName);
        $req2->execute(array());

        return $req2->fetchAll();
    }

    //Select from return fecthAll()
    public function selectSumFromTableGroupByFetchAll($bdd,$column,$tableName,$groupBy)
    {
        $req2=$bdd->prepare('SELECT SUM('.$column.') AS CA,'.$groupBy.' FROM '.$tableName.' GROUP BY '.$groupBy);
        $req2->execute(array());

        return $req2->fetchAll();
    }

    public function selectSumFromTableGroupByMonthFetchAll($bdd,$column,$tableName,$groupBy)
    {
        
        $req2=$bdd->prepare('SELECT SUM('.$column.') AS CA, EXTRACT(MONTH FROM '.$groupBy.') AS MONTH FROM '.$tableName.' GROUP BY EXTRACT(MONTH FROM '.$groupBy.')');
        $req2->execute(array());
    
        return $req2->fetchAll();
    }


    //Select from where return fecthAll()
    public function selectFromTableWhereFetchAll($bdd,$tableName,$column,$values)
    {
        $req2=$bdd->prepare('SELECT * FROM '.$tableName.' WHERE '.$column.'=:'.$column.'');
        $req2->execute($values);

       return $req2->fetchAll();
    }

    //Select from where return fecth()
    public function selectFromTableWhereFetch($bdd,$tableName,$column,$values)
    {
        $req2=$bdd->prepare('SELECT * FROM '.$tableName.' WHERE '.$column.'=:'.$column.'');
        $req2->execute($values);

       return $req2->fetch();
    }

    //Select from where order by return fecth()
     public function selectFromTableWhereOrderByFetch($bdd,$tableName,$column_where,$column_order,$values,$orderBy)
     {
         $req2=$bdd->prepare('SELECT * FROM '.$tableName.' WHERE '.$column_where.'=:'.$column_where.' ORDER BY '.$column_order.' '.$orderBy);
         $req2->execute($values);
 
        return $req2->fetch();
     }

}



?>