<?php
 /*try//Essaye
	{
		$bdd= new PDO('mysql:host=localhost;dbname=eyam', 'root', '');//Connection a la base de données
		array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION);
	}
	catch (Exception $e)//Sinon
	{
		die('Erreur : ' . $e->getMessage());//Affiche un message d'erreur
	}*/


	class Database
    {
            private $Host;
            private $User;
            private $Password;
            private $DbName;
            private $Port;
            public $bdd;
            
            public function __construct()
            {
				$this->Host = 'localhost';        //IP du serveur
				$this->User = 'root';                 //Nom d'utilisateur
				$this->Password = '';  //Mot de passe
				$this->DbName = 'eyam';       //Nom de la base de données
				//$this->Port = '3306';                  //Port de la base de données
			}
			
			public function getConnection()
			{
				$dsn = 'mysql:dbname=' . $this->DbName . ';host=' . $this->Host/*.';port=' . $this->Port*/;
				try 
				{
					$bdd = new PDO($dsn, $this->User, $this->Password,array(\PDO::MYSQL_ATTR_INIT_COMMAND =>  'SET NAMES utf8'));
					$this->bdd = $bdd;
					return $bdd;
				} 
				catch (PDOException $e) 
				{
					echo 'Connexion échouée : ' . $e->getMessage();
				}
				
				return false;
            }
    }


?>