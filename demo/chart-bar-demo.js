// Set new default font family and font color to mimic Bootstrap's default styling
Chart.defaults.global.defaultFontFamily = '-apple-system,system-ui,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif';
Chart.defaults.global.defaultFontColor = '#292b2c';

// Bar Chart Example
$.post("../monitoring/data_ca_by_month.php",
function (data)
{
  console.log(data);
    var montant = [];
    var month = [];
    var tab_month = ["Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet", "Aout", "Septembre", "Octobre", "Novembre", "Décembre"];

    for (var i in data) {
        montant.push(data[i].CA);
        month.push(tab_month[(data[i].MONTH) - 1]);
    }
    var ctx = document.getElementById("myBarChart");
    var myLineChart = new Chart(ctx, {
      type: 'bar',
      data: {
        labels: month,//Abscisse
        datasets: [{
          label: "Revenue",
          backgroundColor: "rgba(2,117,216,1)",
          borderColor: "rgba(2,117,216,1)",
          data: montant,//Ordonnée
        }],
      },
      options: {
        scales: {
          xAxes: [{
            time: {
              unit: 'month'
            },
            gridLines: {
              display: false
            },
            ticks: {
              maxTicksLimit: 2
            }
          }],
         /* yAxes: [{
            ticks: {
              min: 0,
              max: 20000,
              maxTicksLimit: 10
            },
            gridLines: {
              display: true
            }
          }],*/
        },
        legend: {
          display: false
        }
      }
    });
  });
