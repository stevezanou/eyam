<?php
require('Traitements/session_verif.php');
require('Traitements/bdd.php');

include_once 'api/config/core.php';
include_once 'libs/php-jwt/src/BeforeValidException.php';
include_once 'libs/php-jwt/src/ExpiredException.php';
include_once 'libs/php-jwt/src/SignatureInvalidException.php';
include_once 'libs/php-jwt/src/JWT.php';
use \Firebase\JWT\JWT;


if(isset($_POST) && !empty($_POST['email']) && !empty($_POST['password']) && !empty($_POST['nom']) && !empty($_POST['prenom']) && !empty($_POST['date_de_naissance'])) //Si le tableau est défini et ses cases sont non vides
{
  extract($_POST);  //On extrait directement les variables pour ne pas trimballer les $_POST
  # Create a connection
  $url = 'http://localhost/site_web_e-yam_api/api/user/create_user.php';
  $ch = curl_init($url);
  # Form data string
  $postString = json_encode($_POST);
  # Setting our options
  curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
  curl_setopt($ch, CURLOPT_POSTFIELDS, $postString);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
  # Get the response
  $response = curl_exec($ch);
  $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
  var_dump($response);
  echo 'tutut'.$httpcode;
  curl_close($ch);

  /*if($httpcode = 200)
  {
    header('Location: ../article.php?token='.$response);
  }*/
  
}


?>
<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>e-Yam: African Online Market</title>

    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!--On rajoute un lien CSS -->
    <link rel="stylesheet" type="text/css" href="css/style_popup.css">

    <!-- Custom styles for this template -->
    <link href="css/shop-homepage.css" type="text/css" rel="stylesheet">

  </head>

  <body>
  
  <!-- jQuery & Bootstrap 4 JavaScript libraries -->
<script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
 
<!-- jquery scripts will be here -->

    <!-- Navigation -->
    <?php include 'header.php'; ?>


    <!-- Page Content -->

    
    <a href="#" id='sign_up'>Sign Up</a>
    
    <div class="container" id="content">

    
          

      <div class="row">




        <!-- /.col-lg-3 -->

        <div class="col-lg-9">

        <h1>Formulaire d'inscription </h1>
                      <div id="response"></div>

                              <form id="sign_up_form" method="post" action="">
                              
                                  <div class="informations">

                                      <p>
                                        <label for="nom"> Nom: </label>
                                        <input type="text" name="nom" id="nom" placeholder="DEEN" required>
                                      </p>

                                      <p>
                                        <label for="prenom"> Prénom: </label>
                                        <input type="text" name="prenom" id="prenom" placeholder="Tamrath" required>
                                      </p>

                                      <p>
                                        <label for="naissance"> Date de naissance: </label>
                                        <input type="date" name="date_de_naissance" id="naissance">
                                      </p>

                                      <p>
                                        <label for="tel"> Numéro de téléphone: </label>
                                        <input type="tel" name="telephone" id="tel" maxlength="8" placeholder="00000000" required>
                                      </p>
                                      

                                      <p>
                                        <label for="mail"> Votre adresse-mail: </label>
                                        <input type="email" name="email" id="mail" placeholder="tamrathdeen@gmail.com">
                                      </p>

                                      <p>
                                        <!--<label for="adresse"> Adresse: </label> -->
                                        <p>Adresse:</p>
                                        <textarea  name="adresse" id="adresse" placeholder="Fridjrossè vers plage" rows="5" cols="40" required ></textarea>
                                      </p>

                                      <p>
                                        <label for="pays"> Pays de résidence: </label>
                                        <select name="pays" id="pays">
                                          <option value="france"> France </option>
                                          <option value="benin" selected> Bénin </option>
                                          <option value="nigeria"> Nigéria </option>
                                          <option value="cote_d_ivoire"> Côte d'ivoire </option>
                                          <option value="togo"> Togo </option>

                                        </select>  
                                      </p>

                                      <p>
                                        <label for="mot_de_passe"> Mot de passe: </label>
                                        <input type="password" name="password" id="mot_de_passe" maxlength="8" placeholder="********" required>
                                      </p>

                                      

                                      <input type ="hidden" name="type" value="client">

                                      
                                      <button type="submit" class='btn btn-primary'> Créer mon compte </button>

                                  </div>

                            </form>

          <script>
            // jQuery codes
           /* $(document).ready(function()
            {
                // show sign up / registration form
                //$(document).on('click', '#sign_up', function()
                //{
 
                  var html = `

                      <h1>Formulaire d'inscription </h1>
                      <div id="response"></div>

                              <form id="sign_up_form">
                              
                                  <div class="informations">

                                      <p>
                                        <label for="nom"> Nom: </label>
                                        <input type="text" name="nom" id="nom" placeholder="DEEN" required>
                                      </p>

                                      <p>
                                        <label for="prenom"> Prénom: </label>
                                        <input type="text" name="prenom" id="prenom" placeholder="Tamrath" required>
                                      </p>

                                      <p>
                                        <label for="naissance"> Date de naissance: </label>
                                        <input type="date" name="date_de_naissance" id="naissance">
                                      </p>

                                      <p>
                                        <label for="tel"> Numéro de téléphone: </label>
                                        <input type="tel" name="telephone" id="tel" maxlength="8" placeholder="00000000" required>
                                      </p>
                                      

                                      <p>
                                        <label for="mail"> Votre adresse-mail: </label>
                                        <input type="email" name="email" id="mail" placeholder="tamrathdeen@gmail.com">
                                      </p>

                                      <p>
                                        <!--<label for="adresse"> Adresse: </label> -->
                                        <p>Adresse:</p>
                                        <textarea  name="adresse" id="adresse" placeholder="Fridjrossè vers plage" rows="5" cols="40" required ></textarea>
                                      </p>

                                      <p>
                                        <label for="pays"> Pays de résidence: </label>
                                        <select name="pays" id="pays">
                                          <option value="france"> France </option>
                                          <option value="benin" selected> Bénin </option>
                                          <option value="nigeria"> Nigéria </option>
                                          <option value="cote_d_ivoire"> Côte d'ivoire </option>
                                          <option value="togo"> Togo </option>

                                        </select>  
                                      </p>

                                      <p>
                                        <label for="mot_de_passe"> Mot de passe: </label>
                                        <input type="password" name="password" id="mot_de_passe" maxlength="8" placeholder="********" required>
                                      </p>

                                      

                                      <input type ="hidden" name="type" value="client">

                                      
                                      <button type="submit" class='btn btn-primary'> Créer mon compte </button>

                                  </div>

                            </form>
                            `;
 
                        clearResponse();
                        $('#content').html(html);
                   // });
                
                    
                    // trigger when registration form is submitted
                    $(document).on('submit', '#sign_up_form', function(){
                     
                      console.log('on entre');
                    // get form data
                    var sign_up_form=$(this);
                    var form_data=JSON.stringify(sign_up_form.serializeObject());
                    console.log(form_data);
                    // submit form data to api
                    $.ajax({
                        url: "api/user/create_user.php",
                        type : "POST",
                        contentType : 'application/json',
                        data : form_data,
                        success : function(result) {
                            // if response is a success, tell the user it was a successful sign up & empty the input boxes
                            $('#response').html("<div class='alert alert-success'>Inscription réussie. Vous pouvez vous connecter</div>");
                            sign_up_form.find('input').val('');
                        },
                        error: function(jqXHR, textStatus, errorThrown)
                        {
                          
                          console.log("error");
                          console.log(jqXHR);
                          console.log(textStatus);
                          console.log(errorThrown);
                          $('#response').html("<div class='alert alert-danger'>L'inscription à échoué. Contactez l'administrateur</div>");
                        }
                    });

                    return false;
                    });
                
                   
                
                    // clearResponse() will be here
                    // remove any prompt messages
                    function clearResponse(){
                        $('#response').html('');
                    }

                     // show login form trigger will be here

                     
                    // serializeObject will be here
                    // function to make form values to json format
                    $.fn.serializeObject = function(){
                    
                    var o = {};
                    var a = this.serializeArray();
                    $.each(a, function() {
                        if (o[this.name] !== undefined) {
                            if (!o[this.name].push) {
                                o[this.name] = [o[this.name]];
                            }
                            o[this.name].push(this.value || '');
                        } else {
                            o[this.name] = this.value || '';
                        }
                    });
                    return o;
                    };
                });*/
          </script>

        </div>
        <!-- /.col-lg-9 -->

      </div>
      <!-- /.row -->

    </div>
    <!-- /.container -->

    <!-- Footer -->
    <footer class="py-5 bg-dark">

      <div class="bas_page">

      <div class="rubrique"> 

        <h5>Nous connaître</h5>

        <ul>
          <li> Aide </li>
          <li>Contact</li>
          <li>Acheter sur e-yam</li>
          <li>Politique de retour</li>
          <li>Chantal (Notre Bot)</li>
        </ul>
        
      </div>



      <div class="rubrique"> 

        <h5>Devenir partenaire</h5>

        <ul>
          <li> Point relais </li>
          <li>Ambassadeur</li>
          <li><a href="inscription_vendeur.php">Fournisseur</a></li>

        </ul>
        
      </div>




      <div class="rubrique"> 

        <h5>Réseaux sociaux</h5> 

        <ul>
          <li> <img src="Images/facebook.png" class="emoticones"> </li>
          <li> <img src="Images/whatsapp.png" class="emoticones"> </li>
          <li> <img src="Images/twitter.png" class="emoticones"> </li>
          <li> <img src="Images/instagram.png" class="emoticones"> </li>
          <li> <img src="Images/youtube.png" class="emoticones"></li>
        </ul>

      </div>


      <div class="rubrique"> 

        <h5>Mode de paiement</h5>
        

        <ul>
          <li> <img src="Images/billets.png" class="emoticones"> Espèces </li>
          <li> <img src="Images/mobile.png" class="emoticones"> Mobile Money</li>
          <li> <img src="Images/paypal.png" class="emoticones"> Paypal</li>
          <li> <img src="Images/visa.png" class="emoticones"> Visa</li>
          <li> <img src="Images/mastercard.png" class="emoticones"> Master Card</li>
        </ul>


      </div>
        
      <div class="rubrique"> 
        
        <h5> e-Yam international </h5>
        

        <ul>
          <li>France</li>
          <li>Bénin </li>
          <li>Togo</li>
          <li>Nigéria</li>
          <li>Côte d'ivoire</li>
        </ul>

      </div>
 


    </div>


      <div class="container_1">
        <p class="m-0 text-center text-white">Copyright &copy; e-Yam 2018</p>
      </div>  


      <!-- /.container -->
    </footer>

    <!-- Bootstrap core JavaScript -->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- On relie avec notre fichier contenant le javaScript -->
    <script src="code_pup.js" type="text/javascript"></script>



  </body>

</html>
