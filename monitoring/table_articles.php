<?php
  include '../Traitements/bdd.php';

?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Admin:e-Yam</title>
	<!-- On relie au CSS -->
	<link rel="stylesheet" type="text/css" href="../css/admin_eyam.css">
  <link href="../css/style-cart.css" rel="stylesheet">
</head>
<body>
	<!-- On insère la barre de navigation tout en haut -->

	<div class="topnav">
 		
 		<a href="commandes.php">Commandes</a>
 		<a class="active" href="table_articles.php">Articles</a>
 		<a href="graphiques.html">Graphiques</a>
 		<a href="#" class="left"> <img src="../Images/account.svg" class="icone"> </a>
  		<a href="#" class="left"> <img src="../Images/chat.svg" class="icone"> </a>
 		<a href="#" class="left"> <img src="../Images/notifications.svg" class="icone"> </a>
 			
	</div>
	<!-- Fin insertion de la barre de navigation tout en haut -->

<div id="controleArticles">
	<form method="post" action="../Traitements/traitement_monitoring_crud.php" enctype="multipart/form-data">

      <p>
        <label for="id"> NUM_REF </label>
        <input type="text" name="id" id="id" placeholder="Numéro de référence" required>
      </p>

	   <p>
        <label for="price"> PRI_UN </label>
        <input type="text" name="price" id="price" placeholder="Prix unitaire pour la quantité minimale" >
      </p>

      <p>
        <label for="name"> NOM_AR </label>
        <input type="text" name="name" id="name" placeholder="Nom de l'article" >
      </p>

	   <p>
        <label for="stock"> STOC_AR </label>
        <input type="text" name="stock" id="stock" placeholder="Stock disponible " >
      </p>


       <p>
        <label for="description_art"> DESC_AR </label>
        <input type="text" name="description_art" id="description_art" placeholder="Description sommaire de l'article" >
      </p>

	   <p>
        <label for="quantite_min"> QU_MIN </label>
        <input type="text" name="quantite_min" id="quantite_min" placeholder="Quantité minimale vendu" >
      </p>

      <p>
        <label for="categorie"> CATEGORIE </label>
        <select name="categorie" id="categorie">
       	  <option value="1" selected> 1 - Huiles-Épices et Graines </option>
          <option value="2">2 - Féculents-Céréales</option>
          <option value="3" > 3 - Fruits-Légumes </option>
          <option value="4"> 4 - Viandes-Poissons-Oeufs </option>
          <option value="5"> 5 - Divers articles </option>
          <option value="6"> 6 - Liqueurs-eau-boissons sucrées </option>

         </select> 
      </p>

      <p>
        <label for="promo"> PROMOS </label>
        <select name="promo" id="promo">
       	  <option value="0" selected> 0% </option>
          <option value="10"> 10% </option>
          <option value="15" > 15% </option>
          <option value="20"> 20% </option>
          <option value="25"> 25% </option>
          <option value="30"> 30% </option>

         </select> 
      </p>



	   <p>
        <label for="photo"> PHO_AR: </label>
        <input type="file" name="photo" id="photo" />
      </p>


      <input class="bouton_action" type="submit" name="action" value="Ajouter" />
      <input  class="bouton_action" type="submit" name="action" value="Modifier" />
      <input  class="bouton_action" type="submit" name="action" value="Supprimer" />
	</form>

	<!-- Boutons de vaalidaation
	<button class="bouton_action"> Ajouter </button>
	<button class="bouton_action"> Modifier  </button>
  <button class="bouton_action"> Supprimer </button>-->
</div>


<?php
  //On affiche le message correspondant à l'action effectuée
  if(isset($_GET) && !empty($_GET['message']))
  {
    extract($_GET);

    if($message == "1")
    {
      echo 'Ajout effectué avec succès';
    }
    elseif ($message == "2") {
      echo 'Modification effectuée avec succès';
    }
    elseif ($message == "3") {
      echo 'Suppression effectuée avec succès';
    }

  }
  //Requete pour afficher le tableau des articles
  $req=$bdd->prepare('SELECT * FROM article');
	$req->execute(array(
   
  ));
        
   //Debut affichage tableau articles 
   echo '<div class="tableau">
   <table style="width:95%">
     <tr>
       <th class="entete">NUM_REF</th>
       <th class="entete">PHOTOS</th>
       <th class="entete">NOMS </th> 
       <th class="entete">CATEGORIE </th> 
       <th class="entete">PRIX_UNIT</th>
       <th class="entete">STOCKS</th>
       <th class="entete">DESCRIPT</th> 
       <th class="entete">QUANT_MIN</th>
       <th class="entete">PROMO</th>    
     </tr>';
  while ($donnees = $req->fetch())
  {
    

        echo '<tr>
                <td>'.$donnees['id'].'</td>
                <td>
                
                <img id="pan" src="../Images/articles/'.$donnees['photo_article'].'">
                
                </td> 
                <td>'.$donnees['nom_article'].'</td>
                <td>'.$donnees['categorie'].'</td>
                <td>'.$donnees['prix_unitaire'].'</td>
                <td>'.$donnees['stock_article'].'</td> 
                <td>'.$donnees['description'].'</td>
                <td>'.$donnees['quantite_min'].'</td>
                <td>'.$donnees['promotion'].'</td>
              </tr>';
  }
  ?>

 
</table>

</div>

<!-- Fin insertion tableau articles -->
<!-- Footer 
<footer>
	<p> Copyright @ e-Yam 2018 </p>
</footer>-->

</body>
</html>