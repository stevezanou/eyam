<?php
  include '../Traitements/bdd.php';

?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Admin:e-Yam</title>
	<!-- On relie au CSS -->
	<link rel="stylesheet" type="text/css" href="../css/admin_eyam.css">
  <link rel="stylesheet" type="text/css" href="../css/style_graphiques.css">
  <link rel="stylesheet" type="text/css" href="../css/dashboard.css">
</head>
<body>
	<!-- On insère la barre de navigation tout en haut -->

	<div class="topnav">
    
 		
 		<a href="commandes.php">Commandes</a>
 		<a  href="table_articles.php">Articles</a>
 		<a href="graphiques.html">Graphiques</a>
 		<a href="#" class="left"> <img src="../Images/account.svg" class="icone"> </a>
  	<a href="#" class="left"> <img src="../Images/chat.svg" class="icone"> </a>
 		<a href="#" class="left"> <img src="../Images/notifications.svg" class="icone"> </a>
 			
	</div>
	<!-- Fin insertion de la barre de navigation tout en haut -->


<div class="Premiere_Bande">
  <!-- Graphique -->
  

  <div id="toutes_rubriques">


    <div class="rubrique --1">
     <h3> 120 en attente </h3>
    </div>

    <div class="rubrique --2">
     <h3> 40 traitées </h3>
    </div>

    <div class="rubrique --3">
     <h3> 120 livrées </h3>
    </div>

    <div class="rubrique --4">
      <h3> 03 tâches </h3>
    </div>


    <div class="rubrique --5">
      <h3> 20 Messages</h3>
    </div>



  </div>

</div>

<!-- Debut insertion tableau articles -->
<?php

//Requete pour afficher le tableau des articles
$req=$bdd->prepare('SELECT * FROM commande ORDER BY date_cmd DESC');
$req->execute(array());
      
 //Debut insertion tableau articles 
 echo '<div class="tableau">
 <table style="width:95%">
   <tr>
     <th class="entete">NUM_COM</th>
     <th class="entete">MONTANT</th>
     <th class="entete">STATUT </th>  
   </tr>';
   
   
while ($donnees = $req->fetch())
{
  

      echo '<tr>
              <td>'.$donnees['id_cmd'].'</td>
              <td>'.$donnees['montant_cmd'].'</td>
              <td>
                <form method="post" action="">
                  <input type="hidden" name="id" value="'.$donnees['id_cmd'].'">
                  <input type="text" name="statut" >
                </form>                   
              </td> 
            </tr>';
}


?>

 
</table>

</div>

<!-- Fin insertion tableau articles -->



<!-- Footer -->
<div id="bas_page">
	<p> Copyright @ e-Yam 2018 </p>
</div>


    <!-- Page level plugin JavaScript-->
    <script src="../demo/Chart.min.js"></script>
    <!-- Demo scripts for this page-->
    <script src="../demo/chart-area-demo.js"></script>
    <script src="../demo/chart-bar-demo.js"></script>
    <script src="../demo/chart-pie-demo.js"></script>


</body>
</html>