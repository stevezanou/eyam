<?php
  //ZS
  include '../Traitements/bdd.php';
  include '../Traitements/fonctions.php';

  $donnees = new Functions();
  $donnees2 = new Functions();
  $donnees3 = new Functions();

  //Si on effectue une modification du statut de la commande, on modifie la ligne correspondante dans la table
  if (isset($_POST['action']) && $_POST['action']=="change"){
   
    $req=$bdd->prepare('UPDATE commande SET  statut_cmd=:statut_cmd WHERE id_cmd = :id_cmd');
		$req->execute(array(
            'statut_cmd'=>$_POST['status'],
            'id_cmd'=>$_POST['id'],
		));
      
  }

?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <?php
    //Actualise la page 
      /*$page = $_SERVER['PHP_SELF'];
      $sec = "1200";
      header("Refresh: $sec; url=$page");*/
    ?>
    <title>Admin:e-Yam</title>
    <!-- On relie au CSS -->
    <link rel="stylesheet" type="text/css" href="../css/admin_eyam.css">
    <link rel="stylesheet" type="text/css" href="../css/style_graphiques.css">
    <link rel="stylesheet" type="text/css" href="../css/dashboard.css">
    
  </head>
  <body>
    <!-- On insère la barre de navigation tout en haut -->

    <div class="topnav">
      
      
      <a class="active" href="commandes.php">Commandes</a>
      <a  href="table_articles.php">Articles</a>
      <a href="graphiques.html">Graphiques</a>
      <a href="#" class="left"> <img src="../Images/account.svg" class="icone"> </a>
      <a href="#" class="left"> <img src="../Images/chat.svg" class="icone"> </a>
      <a href="#" class="left"> <img src="../Images/notifications.svg" class="icone"> </a>
        
    </div>
    <!-- Fin insertion de la barre de navigation tout en haut -->
    <br/>
    <br/>
    <br/>
    <br/>
    <br/>
    <div>
        <h2>Détails des commandes</h2>
    </div>
  
    <!-- Graphique -->
    

      <div id="toutes_rubriques">


        <div class="rubrique --1">
          <h3> 
            
            <?php
               
                //On compte le nombre de commande au statut en attente
                /*$req2=$bdd->prepare('SELECT * FROM commande WHERE statut_cmd=:statut_cmd');
                $req2->execute(array(
                        'statut_cmd'=>"En attente", 
                ));

                $donnees=$req2->fetchAll();*/
               

                $valeurs=array(
                  'statut_cmd'=>"En attente");

                $donnees = $donnees->selectFromTableWhereFetchAll($bdd,'commande','statut_cmd',$valeurs);
                
                echo count($donnees).' en attente';

            ?> 
          </h3>
        </div>

        <div class="rubrique --2">
          <h3>
            
            <?php
                  //On compte le nombre de commande au statut traité
                  /*$req2=$bdd->prepare('SELECT * FROM commande WHERE statut_cmd=:statut_cmd');
                  $req2->execute(array(
                          'statut_cmd'=>"Traite", 
                  ));

                  $donnees=$req2->fetchAll();*/
                  $valeurs2=array(
                    'statut_cmd'=>"Traite");
  
                  $donnees2 = $donnees2->selectFromTableWhereFetchAll($bdd,'commande','statut_cmd',$valeurs2);
                  

                  echo count($donnees2).' traitées';

              ?> 
        
        
          </h3>
        </div>

        <div class="rubrique --3">
          <h3> 
            <?php
                  //On compte le nombre de commande au statut livré
                  /*$req2=$bdd->prepare('SELECT * FROM commande WHERE statut_cmd=:statut_cmd');
                  $req2->execute(array(
                          'statut_cmd'=>"Livre", 
                  ));

                  $donnees=$req2->fetchAll();*/

                  $valeurs3=array(
                    'statut_cmd'=>"Traite");
  
                  $donnees3 = $donnees3->selectFromTableWhereFetchAll($bdd,'commande','statut_cmd',$valeurs3);


                  echo count($donnees3).' Livrées';

              ?> 
          </h3>
        </div>

       


        <div class="rubrique --5">
          <h3> 20 Messages</h3>
        </div>
      </div>


  <?php

  //Requete pour afficher le tableau des articles
  $req=$bdd->prepare('SELECT * FROM commande WHERE statut_cmd=:statut_cmd ORDER BY date_cmd DESC');
  $req->execute(array(
    'statut_cmd'=>"En attente",
  ));
        
  //Debut insertion tableau articles 
  echo '<div class="tableau">
  <table style="width:95%">
    <tr>
      <th class="entete">NUM_COM</th>
      <th class="entete">DATE</th>
      <th class="entete">HEURE </th> 
      <th class="entete">NOM_PRENOM</th>
      <th class="entete">ADRESSE_LIVRAISON</th>
      <th class="entete">TELEPHONE</th> 
      <th class="entete">PANIER</th>
      <th class="entete">MONTANT</th>    
      <th class="entete">STATUT</th>  
    </tr>';
    
    
  while ($donnees = $req->fetch())
  {
    //$date_heure = explode(" ",$donnees['date_cmd']);

        echo '<tr>
                <td>'.$donnees['id_cmd'].'</td>
                <td>'.$donnees['date_cmd'].'</td> 
                <td>'.$donnees['time_cmd'].'</td>
                <td>'.$donnees['nom_prenom_cmd'].'</td>
                <td>'.$donnees['adresse_cmd'].'</td> 
                <td>'.$donnees['telephone_cmd'].'</td>
                <td>'.$donnees['panier_cmd'].'</td>
                <td>'.$donnees['montant_cmd'].'</td>';
  ?>             
                <td>             
                  <form method='post' action=''>
                    <input type='hidden' name='id' value="<?php echo $donnees["id_cmd"]; ?>" />
                    <input type='hidden' name='action' value="change" />
                    <select name='status' class='quantity' onChange="this.form.submit()">
                      <option <?php if(strcmp($donnees["statut_cmd"],"En attente")==0) echo "selected";?>
                      value="En attente">En attente</option>
                      <option <?php if(strcmp($donnees["statut_cmd"],"Traite")==0) echo "selected";?>
                      value="Traite">Traité</option>
                      <option <?php if(strcmp($donnees["statut_cmd"],"Livre")==0) echo "selected";?>
                      value="Livre">Livré</option>
                    </select>
                  </form>
                </td>
  <?php             
        echo  '</tr>';
  }

  ?>
  
  </table>

  </div>

  <!-- Fin insertion tableau articles -->



  <!-- Footer 
  <footer >
    <p> Copyright @ e-Yam 2018 </p>
  </footer>-->


      <!-- Page level plugin JavaScript-->
      <script src="../demo/Chart.min.js"></script>
      <!-- Demo scripts for this page-->
      <script src="../demo/chart-area-demo.js"></script>
      <script src="../demo/chart-bar-demo.js"></script>
      <script src="../demo/chart-pie-demo.js"></script>


  </body>
</html>


