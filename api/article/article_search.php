<?php
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
 
// include database and object files
//include_once '../config/core.php';
include_once '../../Traitements/bdd.php';
include_once '../../classes/Article.php';
 
// instantiate database and product object
$database = new Database();
$db = $database->getConnection();
 
// initialize object
$article = new Article($db);
 
// get keywords
$keywords=isset($_GET["s"]) ? $_GET["s"] : "";


// query products
$stmt = $article->search($keywords);
$num = $stmt->rowCount();
 
// check if more than 0 record found
if($num>0){
 
    // products array
    $article_arr=array();
    $article_arr["records"]=array();
 
    // retrieve our table contents
    // fetch() is faster than fetchAll()
    // http://stackoverflow.com/questions/2770630/pdofetchall-vs-pdofetch-in-a-loop
    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
        // extract row
        // this will make $row['name'] to
        // just $name only
        extract($row);
 
        $article_item=array(
            "id" => $id,
            "nom" => $nom_article,
            "stock" => $stock_article,
            "photo" => $photo_article,
            "prix" => $prix_unitaire,
            "description" => $description,
            "categorie" => $categorie,
            "quantite_min" => $quantite_min,
            "promotion" => $promotion,
            "categorie_nom"=> $category_name
        );
 
        array_push($article_arr["records"], $article_item);
    }
 
    // set response code - 200 OK
    http_response_code(200);
 
    // show products data
    echo json_encode($article_arr);
}
 
else{
    // set response code - 404 Not found
    http_response_code(404);
 
    // tell the user no products found
    echo json_encode(
        array("message" => "No products found.")
    );
}
?>