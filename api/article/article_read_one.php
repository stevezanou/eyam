<?php
// required headers
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: GET");
header("Access-Control-Allow-Credentials: true");
header('Content-Type: application/json');
 
// include database and object files
include_once '../../Traitements/bdd.php';
include_once '../../classes/Article.php';
 
// get database connection
$database = new Database();
$db = $database->getConnection();
 
// prepare product object
$article = new Article($db);
 
// set ID property of record to read
$article->id = isset($_GET['id']) ? $_GET['id'] : die();
 
// read the details of product to be edited
$article->readOne();
 
if($article->nom_article!=null){
    // create array
    $article_arr = array(
        "id" =>  $article->id,
        "nom_article" => $article->nom_article,
        "stock_article" => $article->stock_article,
        "photo_article" => $article->photo_article,
        "prix_unitaire" => $article->prix_unitaire,
        "description" => $article->description,
        "categorie" => $article->categorie,
        "quantite_min" => $article->quantite_min,
        "quantite_min" => $article->quantite_min,
        "promotion" => $article->promotion
 
    );
 
    // set response code - 200 OK
    http_response_code(200);
 
    // make it json format
    echo json_encode($article_arr);
}
 
else{
    // set response code - 404 Not found
    http_response_code(404);
 
    // tell the user product does not exist
    echo json_encode(array("message" => "Article does not exist."));
}
?>