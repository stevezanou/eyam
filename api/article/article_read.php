<?php
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
 
// database connection will be here

// include database and object files
include_once '../../Traitements/bdd.php';
include_once '../../classes/Article.php';

// instantiate database and product object
$database = new Database();
$db = $database->getConnection();
var_dump($db);
 
// initialize object
$article = new Article($db);

// query products
$stmt = $article->read();
$num = $stmt->rowCount();
echo $num;
 
// check if more than 0 record found
if($num>0){
 
    // products array
    $article_arr=array();
    $article_arr["records"]=array();
 
    // retrieve our table contents
    // fetch() is faster than fetchAll()
    // http://stackoverflow.com/questions/2770630/pdofetchall-vs-pdofetch-in-a-loop
    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
        // extract row
        // this will make $row['name'] to
        // just $name only
        extract($row);
        //var_dump($row);
        $article_item=array(
            "id" => $id,
            "nom" => $nom_article,
            "stock" => $stock_article,
            "photo" => $photo_article,
            "prix" => $prix_unitaire,
            "description" => $description,
            "categorie" => $categorie,
            "quantite_min" => $quantite_min,
            "promotion" => $promotion,
            "categorie_nom"=> $category_name
        );
 
        array_push($article_arr["records"], $article_item);
    }
 
    // set response code - 200 OK
    http_response_code(200);
 
    // show products data in json format
    echo json_encode($article_arr);
}
 
// no products found will be here
else{
 
    // set response code - 404 Not found
    http_response_code(404);
 
    // tell the user no products found
    echo json_encode(
        array("message" => "Erreur 404. Aucun article trouvé")
    );
}