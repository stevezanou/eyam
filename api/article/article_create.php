<?php
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
 
// get database connection
include_once '../../Traitements/bdd.php';
include_once '../../classes/Article.php';
 
$database = new Database();
$db = $database->getConnection();
 
$article = new Article($db);
 
// get posted data
$data = json_decode(file_get_contents("php://input"));
 var_dump($data);

 echo $data->id;
 echo $data->nom_article;
 echo $data->stock_article;
 echo $data->photo_article;
 echo $data->prix_unitaire;
 echo $data->description;
 echo $data->categorie;
 echo $data->quantite_min;
 echo $data->promotion;
// make sure data is not empty
if(
   isset($data)
){
    echo 'on entre';
 
    // set product property values
    $article->id = $data->id;
    $article->nom_article = $data->nom_article;
    $article->stock_article = $data->stock_article;
    $article->photo_article = $data->photo_article;
    $article->prix_unitaire = $data->prix_unitaire;
    $article->description = $data->description;
    $article->categorie = $data->categorie;
    $article->quantite_min = $data->quantite_min;
    $article->promotion = $data->promotion;


   
 
    // create the product
    if($article->create()){
 
        // set response code - 201 created
        http_response_code(201);
 
        // tell the user
        echo json_encode(array("message" => "Article was created."));
    }
 
    // if unable to create the product, tell the user
    else{
 
        // set response code - 503 service unavailable
        http_response_code(503);
 
        // tell the user
        echo json_encode(array("message" => "Unable to create article."));
    }
}
 
// tell the user data is incomplete
else{
 
    // set response code - 400 bad request
    http_response_code(400);
 
    // tell the user
    echo json_encode(array("message" => "Unable to create article. Data is incomplete."));
}
?>