<?php
// required headers
header("Access-Control-Allow-Origin: http://localhost/rest-api-authentication-example/");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");



// files needed to connect to database
include_once '../../Traitements/bdd.php';
include_once '../../classes/User.php';
 
// get database connection
$database = new Database();
$db = $database->getConnection();
 
// instantiate product object
$user = new User($db);
 
// submitted data will be here

// get posted data
$data = json_decode(file_get_contents("php://input"));
var_dump($data);

// set product property values
$user->nom = $data->nom;
$user->prenom = $data->prenom;
$user->date_de_naissance = $data->date_de_naissance;
$user->telephone = $data->telephone;
$user->adresse = $data->adresse;
$user->pays = $data->pays;
$user->email = $data->email;
$user->password = $data->password;
$user->type = $data->type;



// create the user
if($user->create()){
 
    // set response code
    http_response_code(200);
 
    // display message: user was created
    echo json_encode(array("message" => "User was created."));
}
 
// message if unable to create user
else{
 
    // set response code
    http_response_code(400);
 
    // display message: unable to create user
    echo json_encode(array("message" => "Unable to create user."));
}
?>