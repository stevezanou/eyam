<?php
require('Traitements/session_verif.php');
require('Traitements/bdd.php');
include "classes/Article.php";

?>


<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>e-Yam: African Online Market</title>

    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/shop-homepage.css" rel="stylesheet">
    <link href="css/style_articles.css" rel="stylesheet">
    <link href="css/style_popup.css" rel="stylesheet">
    <!-- On rajoute le lien avec le css du chat-->
    <link href="css/tuto_chat.css" rel="stylesheet">

    <link href="css/style-cart.css" rel="stylesheet">

    
   
                 
  </head>

  <body>

    

     <!-- Navigation -->
      
    
     <?php //include 'header.php'; ?>


    <!-- Page Content 
   

    <div class="container">-->

          <?php
           // get database connection
           $database = new Database();
           $bdd = $database->getConnection();

           $article = new Article($bdd);
           
                  echo '<div class="row">';
                    echo  '<div class="tous_articles">';
                    //Partie du panier
                    $status = "";
                    if(isset($_POST['id']) && $_POST['id']!="")
                    {
                      $categorie = $_POST['categorie'];
                      /*$req2=$bdd->prepare('SELECT * FROM article WHERE categorie=:categorie AND id = :id');
                      $req2->execute(array(
                      'categorie'=>$categorie,
                      'id'=>$_POST['id'],
                      ));*/

                      $row=$article->readForCart($categorie, $_POST['id']);
                      
                      
                      //$row = $req2->fetch();
                      $name = $row['nom_article'];
                      $id = $row['id'];
                      $price = $row['prix_unitaire'];
                      $image = $row['photo_article'];

                      $cartArray = array(
                        $id=>array(
                        'name'=>$name,
                        'id'=>$id,
                        'price'=>$price,
                        'quantity'=>1,
                        'image'=>$image)
                      );

                      if(empty($_SESSION["shopping_cart"])) {
                        $_SESSION["shopping_cart"] = $cartArray;
                        $status = "<div class='box'>Produit ajouté à votre panier</div>";
                      }
                      else
                      {
                          $array_keys = array_keys($_SESSION["shopping_cart"]);
                          if(in_array($id,$array_keys)) 
                          {
                            //$status = "<div class='box' style='color:red;'>
                            //Ce produit a déjà été ajouté</div>";	
                            $id['quantity']++;
                          } 
                          else 
                          {

                            $_SESSION["shopping_cart"] = array_merge(
                            $_SESSION["shopping_cart"],
                            $cartArray
                            );
                            $status = "<div class='box'>Produit ajouté à votre panier</div>";
                          }
                    
                      }
                    }
                    include 'header.php';
                 
                  if(isset($_POST['searchData']))
                  {

                     

                      extract($_POST);
                      
                      
                      $keywords = $searchData;
                      $stmt = $article->search($keywords);
                      $num = $stmt->rowCount();
                      
                      //$donnees2 = $stmt->fetch();
                     
                      
                      if($num > 0)
                      {
                        
                        while ($row = $stmt->fetch(PDO::FETCH_ASSOC))
                        {
                                                
                            echo    '<div class="objet" >';
                                
                            echo      '<div class="article " >
                                        <form method="post" action="">
                                        <input type="hidden" name="id" value='.$row['id'].' />
                                        <input type="hidden" name="categorie" value='.$row['categorie'].' />
                                        <input type="hidden" name="searchData" value='.$searchData.' />
                                        <button type="submit" id="close-image" > <img id="Image"  src="data:image/jpeg;base64,'.base64_encode( $row['photo_article'] ).'"></button>
                                        <div id="infos" >
                                            <h1 id="titre" style="font-size:25px; text-transform: uppercase;">'. substr($row['nom_article'],0,10).'</h1>
                                            <h4 id="prix" style="font-size:25px; ">'. $row['prix_unitaire'].' FCFA'.'</h1>
                                        
                                            
                                        </div>
                                        
                                        </form>
                                    </div>';

                            echo    '</div>';
                            //echo '<br/>';
                        
                        }
                       
                   }
                }
                  echo  '</div>';
                  echo  '</div>';

				//}

            //}
          
          
          ?>
            <div style="clear:both;"></div>
 
            <div class="message_box" style="margin:10px 0px;">
            <?php //echo $status; ?>
            </div>
    <!-- </div>
    /.container -->

    <!-- Footer -->
    <footer class="py-5 bg-dark">

      <div class="bas_page">

      <div class="rubrique"> 

        <h5>Nous connaître</h5>

        <ul>
          <li> Aide </li>
          <li>Contact</li>
          <li>Acheter sur e-yam</li>
          <li>Politique de retour</li>
          <li>Chantal (Notre Bot)</li>
        </ul>
        
      </div>



      <div class="rubrique"> 

        <h5>Devenir partenaire</h5>

        <ul>
          <li> Point relais </li>
          <li>Ambassadeur</li>
          <li>Fournisseur</li>

        </ul>
        
      </div>




      <div class="rubrique"> 

        <h5>Réseaux sociaux</h5> 

        <ul>
          <li> <img src="Images/facebook.png" class="emoticones"> </li>
          <li> <img src="Images/whatsapp.png" class="emoticones"> </li>
          <li> <img src="Images/twitter.png" class="emoticones"> </li>
          <li> <img src="Images/instagram.png" class="emoticones"> </li>
          <li> <img src="Images/youtube.png" class="emoticones"></li>
        </ul>

      </div>


      <div class="rubrique"> 

        <h5>Mode de paiement</h5>
        

        <ul>
          <li> <img src="Images/billets.png" class="emoticones"> Espèces </li>
          <li> <img src="Images/mobile.png" class="emoticones"> Mobile Money</li>
          <li> <img src="Images/paypal.png" class="emoticones"> Paypal</li>
          <li> <img src="Images/visa.png" class="emoticones"> Visa</li>
          <li> <img src="Images/mastercard.png" class="emoticones"> Master Card</li>
        </ul>

      </div>
        
      <div class="rubrique"> 
        
        <h5> e-Yam international </h5>
        

        <ul>
          <li>France</li>
          <li>Bénin </li>
          <li>Togo</li>
          <li>Nigéria</li>
          <li>Côte d'ivoire</li>
        </ul>

      </div>
 


    </div>


      <div class="container_1">
        <p class="m-0 text-center text-white">Copyright &copy; e-Yam 2018</p>
      </div>  


      <!-- /.container -->
    </footer>

    <!-- Bootstrap core JavaScript -->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
     <!-- On rajoute ceci pour l'ouverture des popups -->
     <script src="code_pup.js"></script>
     
      
  </body>

</html>
