<?php

//session_start();
require('Traitements/session_verif.php');
require('Traitements/bdd.php');

$status="";
if (isset($_POST['action']) && $_POST['action']=="remove"){
  
  if(!empty($_SESSION["shopping_cart"])) {
    //var_dump($_SESSION["shopping_cart"] );
    //die();
      foreach($_SESSION["shopping_cart"] as $key => $value) {
      // var_dump($value);
        //die();
        if($_POST["id"] == $value['id']){
          //die($_POST["id"]);
        unset($_SESSION["shopping_cart"][$key]);
        
        //die();
        $status = "<div class='box' style='color:red;'>
            Ce produit a été retiré de votre panier</div>";
        }
        if(empty($_SESSION["shopping_cart"]))
        unset($_SESSION["shopping_cart"]);
        }		
  }
}

if (isset($_POST['action']) && $_POST['action']=="change"){
  foreach($_SESSION["shopping_cart"] as &$value){
    if($value['id'] === $_POST["id"]){
        $value['quantity'] = $_POST["quantity"];
        break; // Stop the loop after we've found the product
    }
  }
  	
}

?>


<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>e-Yam: African Online Market</title>

    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/shop-homepage.css" rel="stylesheet">
    <link href="css/style_articles.css" rel="stylesheet">
    <link href="css/style_popup.css" rel="stylesheet">
    <!-- On rajoute le lien avec le css du chat-->
    <link href="css/tuto_chat.css" rel="stylesheet">
    <link href="css/style_panier.css" rel="stylesheet">
    <link href="css/style-cart.css" rel="stylesheet">

  </head>

  <body>

    

     <!-- Navigation -->
      
    
     <?php include 'header.php'; ?>


    <!-- Page Content-->

          
    <!-- Tableau -->
  <div id="contenu_page">
  	
      <h1>MON PANIER</h1>
      <?php
        if(isset($_SESSION["shopping_cart"])){
          
            $total_price = 0;
            $tva=0;
            $basket_price=0;
        ?>
      <table style="width:90%">
    
      <tr>
        <th id="entete">Photo</th>
        <th id="entete">Nom du produit</th> 
        <th id="entete">Prix unitaire</th>
        <th id="entete">Quantité</th>
        <th id="entete">Sous-total</th>
        <th id="entete">Supprimer</th>
      </tr>

      <?php		
        foreach ($_SESSION["shopping_cart"] as $product){
      ?>
      
    
      <tr>
        <td id="ligne"><?php echo '<img id="pan" src="data:image/jpeg;base64,'.base64_encode( $product['image'] ).'">' ?></td>
        <td id="ligne"><?php echo $product["name"]; ?></td> 
        <td id="ligne"><?php echo "$".$product["price"]; ?></td>
        <td id="ligne">
            
        <form method='post' action=''>
          <input type='hidden' name='id' value="<?php echo $product["id"]; ?>" />
          <input type='hidden' name='action' value="change" />
          <select name='quantity' class='quantity' onChange="this.form.submit()">
            <option <?php if($product["quantity"]==1) echo "selected";?>
            value="1">1</option>
            <option <?php if($product["quantity"]==2) echo "selected";?>
            value="2">2</option>
            <option <?php if($product["quantity"]==3) echo "selected";?>
            value="3">3</option>
            <option <?php if($product["quantity"]==4) echo "selected";?>
            value="4">4</option>
            <option <?php if($product["quantity"]==5) echo "selected";?>
            value="5">5</option>
          </select>
          </form>
        </td>
        
        <td id="ligne"><?php echo "$".$product["price"]*$product["quantity"]; ?></td>
        <td id="ligne"> 
          <form method='post' action=''>
            <input type='hidden' name='id' value="<?php echo $product['id']; ?>" />
            <input type='hidden' name='action' value="remove" />
            <button type='submit' class='remove'><img src="Images/poubelle.png" id="icone_suppr"></button>
          </form>
        </td>
      </tr>
          
        <?php
          //
          $total_price += ($product["price"]*$product["quantity"]);
          }
        ?>
      
      </tr>
      <!--<tr>
        <td id="ligne">Jill</td>
        <td id="ligne">Smith</td> 
        <td id="ligne">50</td>
        <td id="ligne">
            
            <form >
                <input type="number" name="quantity" min="1" max="100">
            </form> 
        </td>
        
        <td id="ligne">50</td>
        <td id="ligne"> <img src="Images/poubelle.png" id="icone_suppr"> </td>
      </tr>-->
    
    </table >
    
    <?php
}else{
	echo "<h2>Votre panier est vide</h2>";
	}
?>

<div style="clear:both;"></div>
 
 <div class="message_box" style="margin:10px 0px;">
 <?php echo $status; ?>
 </div>
    <a href="index.php"> <button id="bouton_1"> Continuer mes achats</button><a/>
    
    
    <table id="solde">
          <!-- solde -->
      <tr>
          <td >Sous-total HT</td>
          <td >
            <?php
            
              if(isset($total_price)){
                echo $total_price."€";
              }
              else
              {
                echo "0";
              }
            ?>
          </td>
      </tr>
      
      <tr>
          <td >TVA et autres taxes </td>
          <td > 
            <?php
              $tva;
              if(isset($total_price)){
                $tva= (($total_price/100)*20)."€";
                echo $tva;
              }
              else
              {
                echo "0";
              }
            ?>
          </td>
      </tr>
      
     
      
      <tr>
          <td >TOTAL TTC </td>
          <td >
            <?php
              if (isset($total_price)){
                $basket_price = $tva+$total_price;
                $_SESSION['total_panier']=$basket_price;
                echo $basket_price."€";
              }
              else
              {
                echo "0";
              }
            ?>
          </td>
      
      </tr> 
    </table>
   
    <a href="Traitements/traitement_panier.php"><button id="bouton_2" > Régler mes achats</button></a>
 
  </div>
    

    <!-- Footer -->
    <footer class="py-5 bg-dark">

      <div class="bas_page">

      <div class="rubrique"> 

        <h5>Nous connaître</h5>

        <ul>
          <li> Aide </li>
          <li>Contact</li>
          <li>Acheter sur e-yam</li>
          <li>Politique de retour</li>
          <li>Chantal (Notre Bot)</li>
        </ul>
        
      </div>



      <div class="rubrique"> 

        <h5>Devenir partenaire</h5>

        <ul>
          <li> Point relais </li>
          <li>Ambassadeur</li>
          <li>Fournisseur</li>

        </ul>
        
      </div>




      <div class="rubrique"> 

        <h5>Réseaux sociaux</h5> 

        <ul>
          <li> <img src="Images/facebook.png" class="emoticones"> </li>
          <li> <img src="Images/whatsapp.png" class="emoticones"> </li>
          <li> <img src="Images/twitter.png" class="emoticones"> </li>
          <li> <img src="Images/instagram.png" class="emoticones"> </li>
          <li> <img src="Images/youtube.png" class="emoticones"></li>
        </ul>

      </div>


      <div class="rubrique"> 

        <h5>Mode de paiement</h5>
        

        <ul>
          <li> <img src="Images/billets.png" class="emoticones"> Espèces </li>
          <li> <img src="Images/mobile.png" class="emoticones"> Mobile Money</li>
          <li> <img src="Images/paypal.png" class="emoticones"> Paypal</li>
          <li> <img src="Images/visa.png" class="emoticones"> Visa</li>
          <li> <img src="Images/mastercard.png" class="emoticones"> Master Card</li>
        </ul>

      </div>
        
      <div class="rubrique"> 
        
        <h5> e-Yam international </h5>
        

        <ul>
          <li>France</li>
          <li>Bénin </li>
          <li>Togo</li>
          <li>Nigéria</li>
          <li>Côte d'ivoire</li>
        </ul>

      </div>
 


    </div>


      <div class="container_1">
        <p class="m-0 text-center text-white">Copyright &copy; e-Yam 2018</p>
      </div>  


      <!-- /.container -->
    </footer>

    <!-- Bootstrap core JavaScript -->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
     <!-- On rajoute ceci pour l'ouverture des popups -->
     <script src="code_pup.js"></script>

  </body>

</html>
