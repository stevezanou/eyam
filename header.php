 <!-- jquery scripts will be here -->
<?php
include_once 'api/config/core.php';
include_once 'libs/php-jwt/src/BeforeValidException.php';
include_once 'libs/php-jwt/src/ExpiredException.php';
include_once 'libs/php-jwt/src/SignatureInvalidException.php';
include_once 'libs/php-jwt/src/JWT.php';
use \Firebase\JWT\JWT;

if(isset($_POST) && !empty($_POST['email']) && !empty($_POST['password'])) //Si le tableau est défini et ses cases sont non vides
{

  //echo "on entre";

  extract($_POST);  //On extrait directement les variables pour ne pas trimballer les $_POST
  //J'utilise CURL pour appeler les api
  //D'abord j'appelle l'API du login
  # Create a connection
  $url = 'http://localhost/site_web_e-yam_api/api/user/login.php';
  $ch = curl_init($url);
  # Form data string
  $postString = json_encode($_POST);
  # Setting our options
  curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
  curl_setopt($ch, CURLOPT_POSTFIELDS, $postString);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
  # Get the response
  $response = curl_exec($ch);
  $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
  //L'Api renvoie un json contenant le message de succès et le  token jwt
  //On utilise donc le decode pour avoir l'objet et pouvoir récuperer le token
  $myResponse = json_decode($response);
  $jwt = $myResponse->jwt;

  //var_dump($response);
  //echo 'toto'.$httpcode;

  curl_close($ch);

  
  //Si l'appel de l'api login renvoie un succès
  if($httpcode = 200)
  {
    //il faut maintenant valider le tokent et extraire les différentes informations du user
    //On appelle l'api validate_token qui vérifie le token et renvoie un json contenant les data sur le user et le message
    $url = 'http://localhost/site_web_e-yam_api/api/config/validate_token.php';
    $ch = curl_init($url);
    # Form data string
    $postString = $response;
    # Setting our options
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
    curl_setopt($ch, CURLOPT_POSTFIELDS, $postString);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    # Get the response
    $result = curl_exec($ch);
    $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

    //var_dump($result);

    //echo 'tutu'.$httpcode;

    curl_close($ch);
    //Si l'appel de l'api est un succès
    if($httpcode = 200)
    {
      //On decode le resultat qui est encodé en Json
      //Et on récupère les data du user
      $myData = json_decode($result);
      $data = $myData->data;
      var_dump($data);
      
      //Vu que data est un objet, on ne peut pas le transferer directement via url
      //Il faut passer par une variable de session
      session_start();
      $_SESSION['data'] = $data;
      $_SESSION['email']=$data->email;
      $_SESSION['nom']=$data->nom;
      $_SESSION['prenom']=$data->prenom;
      $_SESSION['isAdmin']=$data->isAdmin;
      $_SESSION['logged']=true;
      $_SESSION['type']=$data->type;

      //Si le user est un client (il veut acheter sur le site)
      //On le redirige vers la page où il y a les catégories
      if($data->type == "client")
      {
        header('Location: index.php?message=2');
      }
      else{

			  header('Location: article.php');

      }

    }

    
   
  }
  
}
?>


<!-- Navigation -->
      
    
<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">

<div class="container">
  <a class="navbar-brand" href="index.php">Nous livrons à domicile !</a>

<!-- le champ de recherche et le bouton OK! -->
<div class="searchbar">

   <form method="post" action="recherche.php">

        <input class="champ" type="text" name="searchData" placeholder="Rechercher un produit..." maxlength="70">
        <button class= "input" > Chercher </button>
        
</form>  
        
</div>



  <div class="collapse navbar-collapse" id="navbarResponsive">
    <ul class="navbar-nav ml-auto">
      <li class="nav-item active">
        <a class="nav-link" href="index.php">Accueil
          <span class="sr-only">(current)</span>
        </a>
      </li>
      <li class="nav-item">
        <?php 
          if(!empty($_SESSION["shopping_cart"])) 
          {
            $cart_count = count(array_keys($_SESSION["shopping_cart"]) );
            //Si vendeur l'affichage change
            if(isset($_SESSION['type']) && $_SESSION['type'] =="vendeur")
            {
              echo '<div class="cart_div">
                    <a class="nav-link" href="boutique_vendeur.php">Boutique<span>'.$cart_count.'</span></a>
                  </div>';
            }
            else
            {
            echo '<div class="cart_div">
                    <a class="nav-link" href="panier.php">Panier<span>'.$cart_count.'</span></a>
                  </div>';
            }
          }
          else
          {
            if(isset($_SESSION['type']))
            {
              //Si vendeur l'affichage change
              if($_SESSION['type'] == "vendeur")
              {
                echo '<a class="nav-link" href="boutique_vendeur.php">Boutique </a>';
              }
              else
              {
                echo '<a class="nav-link" href="panier.php">Panier</a>';
              }
            }
            
          }
        ?>
        <!--<a class="nav-link" href="panier.php">Panier </a>-->
      </li>
      <?php
        // Si le user n'est pas connecté, la variable de session vaut false
         if($_SESSION['logged'] == false)
         {
           //echo $_SESSION['logged'];
          echo '<li class="nav-item">
                <a class="nav-link" href="#" onmouseover="openModal()">Votre compte</a>
                </li>';
         }
         //Si connecté et pas admin et pas vendeur
         else if($_SESSION['logged'] == true && $isAdmin ==0 && $_SESSION['type'] !="vendeur")
         {
         // echo $_SESSION['logged'];
          echo '<li class="nav-item">
                <a class="nav-link" href="monCompte.php">'.$nom.' '.$prenom.'</a>
                </li>';

          echo  '<li class="nav-item">
                 <a class="nav-link" href="deconnexion.php">Deconnexion</a>
                 </li>';
         }
         //Si connecté et admin et pas vendeur
         else if($_SESSION['logged'] == true && $isAdmin ==1 && $_SESSION['type'] !="vendeur")
         {
          echo '<li class="nav-item">
          <a class="nav-link" href="monCompte.php">'.$nom.'</a>
          </li>';

          echo '<li class="nav-item">
          <a class="nav-link" href="monCompte.php">Dashboard</a>
          </li>';

          echo  '<li class="nav-item">
                <a class="nav-link" href="deconnexion.php">Deconnexion</a>
                </li>';

         }
         //Si connecté et  vendeur
         else if($_SESSION['logged'] == true && $_SESSION['type'] =="vendeur")
         {
         // echo $_SESSION['logged'];
          echo '<li class="nav-item">
                <a class="nav-link" href="monCompte.php">Dashboard</a>
                </li>';

          echo  '<li class="nav-item">
                 <a class="nav-link" href="deconnexion.php">Deconnexion</a>
                 </li>';
         }

      ?>
      <!--<li class="nav-item">
        <a class="nav-link" href="#">Contact</a>
      </li>-->
      

    </ul>
  </div>
</div>
</nav>

<!-- Page Content -->

<!-- Création du mdodal ou popup -->

<div id="modal" >

<button id="login" onclick="openLogSite()"> LOGIN </button>

<!-- Informations à renseigner -->

<form method="post" action="traitement.php">
<div class="info_popup">
  <p>Nouveau client?</p>
  <a href="inscription.php" id='sign_up'> Cliquer ici</a>
  <h4>Suivre sa commande ici</h4>
  <p>
        <p> Numéro de commande: </p>
        <input type="text" name="ordernumber"  placeholder="12300">
      </p>

      <p>
        <p> Votre adresse-mail: </p>
        <input type="email" name="mail" placeholder="tamrathdeen@gmail.com">
      </p>


</div>

<!-- Soumission -->

<button id="close" > Envoyer </button>


</form>


</div>

<!-- Création du popup de login -->

<div id="log">

<h4> Connexion </h4>
                <div id="return"></div>
                <!-- Formulaire de log -->
                <form id="login_form" method="post" action ="">

                    <p>
                      <p> Votre adresse-mail: </p>
                      <input type="email" name="email" placeholder="tamrathdeen@gmail.com">
                    </p>

                    <p>
                      <p> Mot de passe: </p>
                      <input type="password" name="password"  maxlength="8" placeholder="********" required>
                    </p>

                    <button type="submit" class='btn btn-primary' id="bouton_connect"> Se connecter </button>
                </form>`;

              


</div>


  <!-- Modal 
  
  <div id="myModaly">

      <p>
          Résultat
      </p>
      <p id="search-result-container" ></p>
      

  </div>-->