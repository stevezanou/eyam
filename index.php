<?php
require('Traitements/session_verif.php');

?>

<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>e-Yam: African Online Market</title>

    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/shop-homepage.css" rel="stylesheet">
    <!-- On rajoute le lien avec le css des popups-->
    <link href="css/style_popup.css" rel="stylesheet">
    <!-- On rajoute le lien avec le css du chat-->
    <link href="css/tuto_chat.css" rel="stylesheet">
    <link href="css/style-cart.css" rel="stylesheet">
 
  </head>

  <body>

    <?php include 'header.php'; ?>
    
    

  <!-- Fin  Création  popup login -->

    <!-- L'icone de chat -->
    
   <!-- Sert à mettre le chat
   <p>
       <img src="Images/messages.png" id="suges_chat" onmouseover ="openChat()">
    </p>-->


<!-- Contenu de la page -->



    <div class="container" onmouseover="closeModal()" >





          <div class="row">

            <div class="col-lg-4 col-md-6 mb-4">

              <div class="card h-100">
                <a href="article.php?categorie=1"><img class="card-img-top" src="Images/Huiles.PNG" alt=""></a>
                <div class="card-body">
                  <h4 class="card-title">
                    <a href=<?php echo 'article.php?categorie=1';  ?>>Huiles-Epices-Graines</a>
                  </h4>
                 
                  <p class="card-text">Ici vous trouverez: Huile de palme, huile d'arachides, piment, ail, gingembre, poivre et bien d'autres produits !</p>
                </div>
                <div class="card-footer"></div>
              </div>
            </div>

            <div class="col-lg-4 col-md-6 mb-4">
              <div class="card h-100">
                <a href="article.php?categorie=2"><img class="card-img-top" src="Images/Feculents_Cereales.PNG" alt="" ></a>
                <div class="card-body">
                  <h4 class="card-title">
                    <a href=<?php echo 'article.php?categorie=2';  ?>>Féculents-Cereales</a>
                  </h4>
                  
                  <p class="card-text">Ici vous trouverez: ignames, riz, maïs, manioc, patates douces, mil, sorgho, blé, produits à base de blé et bien d'autres produits semblables !</p>
                </div>
                <div class="card-footer"></div>
              </div>
            </div>

            <div class="col-lg-4 col-md-6 mb-4">
              <div class="card h-100">
                <a href="article.php?categorie=3"><img class="card-img-top" src="Images/Fruits_Legumes.PNG" alt="" ></a>
                <div class="card-body">
                  <h4 class="card-title">
                    <a href=<?php echo 'article.php?categorie=3';  ?>>Fruits-Légumes</a>
                  </h4>
                  
                  <p class="card-text">Ici vous trouverez: de la mangue, des oranges, de l'épinard, des tomates et bien d'autres produits !</p>
                </div>
                <div class="card-footer"></div>
              </div>
            </div>

            <div class="col-lg-4 col-md-6 mb-4">
              <div class="card h-100">
                <a href="article.php?categorie=4"><img class="card-img-top" src="Images/Viandes_Poissons.PNG" alt="" ></a>
                <div class="card-body">
                  <h4 class="card-title">
                    <a href=<?php echo 'article.php?categorie=4';  ?>>Viandes-Poissons-Oeufs</a>
                  </h4>
                  
                  <p class="card-text">Ici vous trouverez: viandes de boeuf, mouton, agneau, dinde, poulet, oeufs, tilapia et bien d'autres produits semblables.</p>
                </div>
                <div class="card-footer"></div>
              </div>
            </div>

            <div class="col-lg-4 col-md-6 mb-4">
              <div class="card h-100">
                <a href="article.php?categorie=5"><img class="card-img-top" src="Images/Divers.PNG" alt="" ></a>
                <div class="card-body">
                  <h4 class="card-title">
                    <a href=<?php echo 'article.php?categorie=5';  ?>>Divers articles</a>
                  </h4>
                  
                  <p class="card-text">Ici vous trouverez tout ce dont vous avez besoin pour votre ménage: éponges, assiettes, draps bien d'autres produits !</p>
                </div>
                <div class="card-footer"></div>
              </div>
            </div>

            <div class="col-lg-4 col-md-6 mb-4">
              <div class="card h-100">
                <a href="article.php?categorie=6"><img class="card-img-top" src="Images/Boissons.PNG" alt="" ></a>
                <div class="card-body">
                  <h4 class="card-title">
                    <a href=<?php echo 'article.php?categorie=6';  ?>>Liqueurs-eau-boissons sucrées</a>
                  </h4>
                  
                  <p class="card-text"> Ici vous trouverez: sodabi, boissons alcolisées boissons sucrées, eau et bien d'autres produits !</p>
                </div>
                <div class="card-footer"></div>
              </div>
            </div>

          </div>
          <!-- /.row -->

        </div>
        <!-- /.col-lg-9 -->

      </div>
      <!-- /.row -->


<!-- Le chat proprement dit -->



<!-- Le chat vient de se terminer ici-->



    </div>
    <!-- /.container -->






    <!-- Footer -->
    <footer class="py-5 bg-dark">

      <div class="bas_page">

      <div class="rubrique"> 

        <h5>Nous connaître</h5>

        <ul>
          <li> Aide </li>
          <li>Contact</li>
          <li>Acheter sur e-yam</li>
          <li>Politique de retour</li>
          <li>Chantal (Notre Bot)</li>
        </ul>
        
      </div>



      <div class="rubrique"> 

        <h5>Devenir partenaire</h5>

        <ul>
          <li> Point relais </li>
          <li>Ambassadeur</li>
          <li><a href="inscription_vendeur.php">Fournisseur</a></li>

        </ul>
        
      </div>




      <div class="rubrique"> 

        <h5>Réseaux sociaux</h5> 

        <ul>
          <li> <img src="Images/facebook.png" class="emoticones"> </li>
          <li> <img src="Images/whatsapp.png" class="emoticones"> </li>
          <li> <img src="Images/twitter.png" class="emoticones"> </li>
          <li> <img src="Images/instagram.png" class="emoticones"> </li>
          <li> <img src="Images/youtube.png" class="emoticones"></li>
        </ul>

      </div>


      <div class="rubrique"> 

        <h5>Mode de paiement</h5>
        

        <ul>
          <li> <img src="Images/billets.png" class="emoticones"> Espèces </li>
          <li> <img src="Images/mobile.png" class="emoticones"> Mobile Money</li>
          <li> <img src="Images/paypal.png" class="emoticones"> Paypal</li>
          <li> <img src="Images/visa.png" class="emoticones"> Visa</li>
          <li> <img src="Images/mastercard.png" class="emoticones"> Master Card</li>
        </ul>

      </div>
        
      <div class="rubrique"> 
        
        <h5> e-Yam international </h5>
        

        <ul>
          <li>France</li>
          <li>Bénin </li>
          <li>Togo</li>
          <li>Nigéria</li>
          <li>Côte d'ivoire</li>
        </ul>

      </div>
 


    </div>


      <div class="container_1">
        <p class="m-0 text-center text-white">Copyright &copy; e-Yam 2018</p>
      </div>  


      <!-- /.container -->
    </footer>

    <!-- Bootstrap core JavaScript -->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- On rajoute ceci pour l'ouverture des popups -->
    <script src="code_pup.js"></script>
    <!-- On rajoute le lien vers le code js du chat  -->
    <script src="tuto_chat.js"></script>

  </body>

</html>
