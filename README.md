Cette procédure a été faite avec un environnement Windows.
Pour recuperer le projet, il faut s'assurer d'avoir Wamp/Lamp/Mamp d'installé sur votre environnement.

Pour télécharger et installer Wamp https://craym.eu/tutoriels/developpement/site_local_avec_wamp.html 
Pour télécharger et installer Mamp https://apprendre-php.com/tutoriels/tutoriel-24-installation-et-prise-en-main-de-mamp.html
Pour télécharger et installer Lamp https://www.hostinger.fr/tutoriels/lamp-ubuntu/ 

Une fois l'environnement installé et déployé sur votre PC, vous pouvez cloner le projet dans votre répertoire sous C:\wamp64(ou wamp)\www\ 

Les différentes API sont créées dans le dossier API du projet. Le dossier comporte à l'heure actuelle une api pour 

	- les articles
		*ajouter article
		*lister tous les articles
		*renvoyer un article en particulier
		*chercher un article
	- les categories
		*lister toutes les categories
	- les users
		*inscription (create user)
		*connexion (login)

L'API renvoie le code HTTP 200/201 si l'appel est reussi et renvoie 404/401/400 ... dans le cas contraire. 



****************************************************************PARTIE BDD***********************************************************************************************************************************************
Le fichier .sql est dans le dossier file sql.
Lorsque vous aurez intallé Wamp/Lamp/Mamp vous pouvez suivre ce tutoriel https://help.one.com/hc/fr/articles/115005588189-Comment-importer-une-base-de-donn%C3%A9es-%C3%A0-phpMyAdmin- pour importer le fichier.
Pensez au préalable à créer une base de données nommée 'eyam'. Vous aurez accès à toutes les tables.